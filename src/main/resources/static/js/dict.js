function showDictSelect(id, type, addOption, selected, ...exclude) {
    var data = getDict(type);
    var select = $("#" + id);
    select.empty();

    if (addOption) {
        if (selected) {
            select.append("<option value=''>" + addOption + "</option>");
        } else {
            select.append("<option value='' selected>" + addOption + "</option>");
        }
    }

    $.each(data, function(k, v) {
        if (exclude.length === 0 || $.inArray(k, exclude) === -1) {
            if (selected && k == selected) {
                select.append("<option value ='" + k + "' selected>" + v + "</option>");
            } else {
                select.append("<option value ='" + k + "'>" + v + "</option>");
            }
        }
    });

    return data;
}

function getDict(type) {
    var v = sessionStorage[type];
    if (v == null || v == "") {
        $.ajax({
            type : 'get',
            url : '/dicts?type=' + type,
            async : false,
            success : function(data) {
                v = {};
                $.each(data, function(i, d) {
                    v[d.k] = d.val;
                });

                sessionStorage[type] = JSON.stringify(v);
            }
        });
    }

    return JSON.parse(sessionStorage[type]);
}
