//form序列化为json
$.fn.serializeObject = function () {
    var o = {};
    var a = this.serializeArray();
    $.each(a, function () {
        var name = this.name;
        // 判断是否是对象
        if (name.indexOf('[') > -1 && name.indexOf(']') > -1) { // 如果是子表数据
            var subListName = name.substring(0, name.indexOf('[')); // 子对象列表在主对象中的名称
            var index = name.substring(name.indexOf('[') + 1, name.indexOf(']')); // 子对象在列表中的index
            var attrName = name.substring(name.indexOf('.') + 1); // 子对象字段名

            // 判断子列表是否存在
            if (o[subListName] == undefined) { // 如果不存在子列表
                o[subListName] = []; // 创建子列表
                var subObj = {}; // 创建子对象
                subObj[attrName] = this.value || ''; // 给子对象的属性赋值
                o[subListName][index] = subObj; // 子列表的index位置指向子对象
            } else if (Array.isArray(o[subListName])) { // 如果已存在子列表
                var subObj = o[subListName][index]; // 尝试获取index位置的子对象
                if (subObj == undefined) { // 如果子对象为空
                    subObj = {}; // 创建子对象
                    subObj[attrName] = this.value || ''; // 给子对象的属性赋值
                    o[subListName][index] = subObj; // 子列表的index位置指向子对象
                } else { // 如果在index位置已有子对象
                    if (subObj[attrName] == undefined) { // 子对象的属性为空
                        subObj[attrName] = this.value || ''; // 给子对象的属性赋值
                    } else { // 子对象的属性不为空
                        if (!Array.isArray(subObj[attrName])) {// 如果子对象的属性不是数组，转成数组
                            subObj[attrName] = [subObj[attrName]];
                        }
                        subObj[attrName].push(this.value || '');// 在数组中追加
                    }
                }
            }
        } else if (name.indexOf('.') > -1 && name.indexOf('[') < 0 && name.indexOf(']') < 0) {// 如果是主表对象，判断对象name是否存在
            var arr = name.split('.');
            var objName = arr[0];
            var attrName = arr[1];
            if (o[objName] === undefined) {// 如果不存在，创建对象name
                o[objName] = {};
                o[objName][attrName] = this.value || '';
            } else {// 如果存在，判断对象中属性是否存在
                if (o[objName][attrName] === undefined) {// 如果不存在，创建属性
                    o[objName][attrName] = this.value || '';
                } else {// 如果存在，判断是否是数组
                    if (!Array.isArray(o[objName][attrName])) {// 如果不是数组，转成数组
                        o[objName][attrName] = [o[objName][attrName]];
                    }
                    // 在数组中追加
                    o[objName][attrName].push(this.value || '');
                }
            }
        } else { // 如果不是对象，判断属性name是否存在
            if (o[name] !== undefined) {// 如果已存在，判断是否是数组
                // 如果不是数组，则转换为数组，在数组中追加
                if (!Array.isArray(o[name])) {
                    o[name] = [o[name]];
                }
                o[name].push(this.value || '');
            } else {// 如果不存在
                o[name] = this.value || '';
            }
        }
    });
    return o;
};

//获取url后的参数值
function getUrlParam(key) {
    const query = window.location.search.substring(1);
    const vars = query.split("&");
    for (let i = 0; i < vars.length; i++) {
        let pair = vars[i].split("=");
        if (pair[0] == key) {
            return decodeURI(pair[1]).trim();
        }
    }
    return (false);
}

// 检查登录状态
function loginInfo() {
    let user = "";
    $.ajax({
        type: 'get',
        url: '/sys/login',
        async: false,
        success: function (data) {
            if (data) {
                user = data;
            }
        },
        error: function (xhr, textStatus, errorThrown) {
            const msg = xhr.responseText;
            const response = JSON.parse(msg);
            $("#info").html(response.message);
        }
    });

    return user;
}

/*
 显示树
 elementId : 树节点的页面元素ID
 elementName : 树节点的页面元素name
 entityName : 实体类英文名，用于拼接初始化树结构方法的路径和已选择节点数据方法的路径
 entityComment : 实体类弹出层title
 treeRootName : 根节点的名称，用于ztree插件的展示
 mode : select/check，用于区分单选树和多选树，默认为select
 isAll : true/false，是否根据数据范围展示数据，true时展示全部数据，部门表必须展示全部数据，不适用此选项
 notAllowSelectParent : true/false，是否可选父节点
  */
function showTree(elementId, elementName, entityName, entityComment, treeRootName, mode, isAll, notAllowSelectParent) {
    isAll = !!isAll;
    notAllowSelectParent = !!notAllowSelectParent;
    layer.open({
        type: 2,
        title: entityComment + '列表',
        // closeBtn: 1, //0为不显示关闭按钮
        // shade: [0], //0为不显示阴影
        shadeClose: true, //是否点击阴影关闭层
        area: ['300px', ($(window).height() - 30) + 'px'],
        offset: 'auto', //rb右下角弹出
        // time: 2000, //2秒后自动关闭
        anim: 0,
        content: ['/pages/tree.html?' +
        'elementId=' + elementId +
        '&elementName=' + elementName +
        '&entityName=' + entityName +
        '&treeRootName=' + treeRootName +
        '&mode=' + mode +
        '&isAll=' + isAll +
        '&notAllowSelectParent=' + notAllowSelectParent], //iframe的url，no代表不显示滚动条
        // end: function(){ //此处用于演示
        //     layer.open({
        //         type: 2,
        //         title: '很多时候，我们想最大化看，比如像这个页面。',
        //         shadeClose: true,
        //         shade: false,
        //         maxmin: true, //开启最大化最小化按钮
        //         area: ['893px', '600px'],
        //         content: '//fly.layui.com/'
        //     });
        // }
        fixed: true, // 是否固定
        maxmin: true,
        btn: ['确定', '清除', '取消'],
        yes: function (index, layero) {
            if (isSelected) {
                $('#' + elementId).val(window[elementId]);
                $('#' + elementName).val(window[elementName]);
                if ($("#form").data('bootstrapValidator')) {
                    // $("#form").data('bootstrapValidator').destroy();
                    $("#form").data('bootstrapValidator').resetForm();
                }
                // $("#form").data('bootstrapValidator').validateField(elementName);
            }
            isSelected = false;
            layer.close(index); //如果设定了yes回调，需进行手工关闭
        },
        cancel: function (index, layero) {
            window[elementId] = undefined;
            window[elementName] = undefined;
            if ($("#form").data('bootstrapValidator')) {
                // $("#form").data('bootstrapValidator').destroy();
                $("#form").data('bootstrapValidator').resetForm();
            }
        },
        btn2: function (index, layero) {
            window[elementId] = 0;
            window[elementName] = treeRootName;
            $('#' + elementId).val('');
            $('#' + elementName).val('');
            if ($("#form").data('bootstrapValidator')) {
                $("#form").data('bootstrapValidator').resetForm();
            }
        },
        btn3: function (index, layero) {
            if ($("#form").data('bootstrapValidator')) {
                $("#form").data('bootstrapValidator').resetForm();
            }
        }
    });
}


/*
 显示树
 elementId : 树节点的页面元素ID
 elementName : 树节点的页面元素name
 entityName : 实体类英文名，用于拼接初始化树结构方法的路径和已选择节点数据方法的路径
 entityComment : 实体类弹出层title
 treeRootName : 根节点的名称，用于ztree插件的展示
 mode : select/check，用于区分单选树和多选树，默认为select
 isAll : true/false，是否根据数据范围展示数据，true时展示全部数据，部门表必须展示全部数据，不适用此选项
 fun : 执行的后台方法
 */
function showSpecialTree(elementId, elementName, entityName, entityComment, treeRootName, mode, fun, isAll, notAllowSelectParent) {
    isAll = !!isAll;
    notAllowSelectParent = !!notAllowSelectParent;
    layer.open({
        type: 2,
        title: entityComment + '列表',
        // closeBtn: 1, //0为不显示关闭按钮
        // shade: [0], //0为不显示阴影
        shadeClose: true, //是否点击阴影关闭层
        area: ['300px', ($(window).height() - 30) + 'px'],
        offset: 'auto', //rb右下角弹出
        // time: 2000, //2秒后自动关闭
        anim: 0,
        content: ['/pages/specialTree.html?' +
        'elementId=' + elementId +
        '&elementName=' + elementName +
        '&entityName=' + entityName +
        '&treeRootName=' + treeRootName +
        '&mode=' + mode +
        '&fun=' + fun +
        '&isAll=' + isAll +
        '&notAllowSelectParent=' + notAllowSelectParent],
        //iframe的url，no代表不显示滚动条
        // end: function(){ //此处用于演示
        //     layer.open({
        //         type: 2,
        //         title: '很多时候，我们想最大化看，比如像这个页面。',
        //         shadeClose: true,
        //         shade: false,
        //         maxmin: true, //开启最大化最小化按钮
        //         area: ['893px', '600px'],
        //         content: '//fly.layui.com/'
        //     });
        // }
        fixed: true, // 是否固定
        maxmin: true,
        btn: ['确定', '清除', '取消'],
        yes: function (index, layero) {
            if (isSelected) {
                $('#' + elementId).val(window[elementId]);
                $('#' + elementName).val(window[elementName]);
                if ($("#form").data('bootstrapValidator')) {
                    // $("#form").data('bootstrapValidator').destroy();
                    $("#form").data('bootstrapValidator').resetForm();
                }
                // $("#form").data('bootstrapValidator').validateField(elementName);
            }
            isSelected = false;
            layer.close(index); //如果设定了yes回调，需进行手工关闭
        },
        cancel: function (index, layero) {
            window[elementId] = undefined;
            window[elementName] = undefined;
            if ($("#form").data('bootstrapValidator')) {
                // $("#form").data('bootstrapValidator').destroy();
                $("#form").data('bootstrapValidator').resetForm();
            }
        },
        btn2: function (index, layero) {
            window[elementId] = 0;
            window[elementName] = treeRootName;
            $('#' + elementId).val('');
            $('#' + elementName).val('');
            if ($("#form").data('bootstrapValidator')) {
                $("#form").data('bootstrapValidator').resetForm();
            }
        },
        btn3: function (index, layero) {
            if ($("#form").data('bootstrapValidator')) {
                $("#form").data('bootstrapValidator').resetForm();
            }
        }
    });
}

/*
 显示表格，根据选择的行刷新同一表单其他字段其他字段
 module: 模块名
 entityName: 实体类英文名
 entityComment: 实体类中文名
 isAll: true/false，是否根据数据范围展示数据，true时展示全部数据
 isMultiSelect: true/false，是否允许多选
 flushFields: 回写的数据，以'本表单元素id:table页元素id'依次排列在参数末尾
  */
function showTableAndFlushFields(module, entityName, entityComment, isAll, isMultiSelect, ...flushFields) {
    let path = '/pages/' + module + '/' + entityName + 'Table.html' +
        '?isAll=' + isAll +
        '&isMultiSelect=' + isMultiSelect;
    if (flushFields) {
        let echo = []; //传输的数组
        let count = 0; //传输的数组长度
        let val = $('#' + flushFields[0].split(':')[0]).val().trim();
        if (val) {
            count = val.split(';').length;
        }

        for (let i = 0; i < count; i++) {
            let item = {};
            $.each(flushFields, function (j, field) {
                let formField = field.split(':')[0];
                let entityField = field.split(':')[1];
                item[entityField] = $('#' + formField).val().split(';')[i];
            });
            echo[i] = item;
        }

        path += '&echo=' + encodeURIComponent(JSON.stringify(echo));
    }

    layer.open({
        type: 2,
        title: entityComment + '列表',
        shadeClose: true, //是否点击阴影关闭层
        area: [($(window).width() * 0.8) + 'px', ($(window).height() - 30) + 'px'],
        offset: 'auto', //rb右下角弹出
        anim: 0,
        content: [path, 'yes'], //iframe的url，no代表不显示滚动条
        fixed: true, // 是否固定
        maxmin: true,
        btn: ['确定', '清除', '取消'],
        yes: function (index, layero) {
            // let rows = layero.find("iframe")[0].contentWindow.example.rows('.success').data();
            let selectedItems = $(layero.find("iframe")[0]).contents().find('[name=selectedItem]');
            if (flushFields) {
                $.each(selectedItems, function (rowIndex, item) {
                    $.each(flushFields, function (fieldIndex, flushFiled) {
                        let fieldsPair = flushFiled.split(':');
                        if (rowIndex === 0) {
                            $('#' + fieldsPair[0].trim()).val($(item).data(fieldsPair[1].trim()));
                        } else {
                            $('#' + fieldsPair[0].trim()).val($('#' + fieldsPair[0].trim()).val().trim() + ';' + $(item).data(fieldsPair[1].trim()));
                        }
                    });
                })
            }
            layer.close(index);
        },
        btn2: function (index, layero) {
            if (flushFields) {
                $.each(flushFields, function (index, item) {
                    $('#' + item.split(':')[0]).val('');
                });
            }
        },
        cancel: function (index, layero) {
            if ($("#form").data('bootstrapValidator')) {
                $("#form").data('bootstrapValidator').resetForm();
            }
        },
        end: function () {
            if ($("#form").data('bootstrapValidator')) {
                $("#form").data('bootstrapValidator').resetForm();
            }
        }
    });
}

// 显示表格，关闭后根据选择的行刷新其他表单（一般为子表）数据
function showTableAndFlushRow(elementName, module, entityName, entityComment) {
    layer.open({
        type: 2,
        title: entityComment + '列表',
        // closeBtn: 1, //0为不显示关闭按钮
        // shade: [0], //0为不显示阴影
        shadeClose: true, //是否点击阴影关闭层
        area: [($(window).width() * 0.8) + 'px', ($(window).height() - 30) + 'px'],
        offset: 'auto', //rb右下角弹出
        // time: 2000, //2秒后自动关闭
        anim: 0,
        content: ['/pages/' + module + '/' + entityName + 'Table.html?elementName=' + elementName, 'yes'], //iframe的url，no代表不显示滚动条
        // end: function(){ //此处用于演示
        //     layer.open({
        //         type: 2,
        //         title: '很多时候，我们想最大化看，比如像这个页面。',
        //         shadeClose: true,
        //         shade: false,
        //         maxmin: true, //开启最大化最小化按钮
        //         area: ['893px', '600px'],
        //         content: '//fly.layui.com/'
        //     });
        // }
        fixed: true, // 是否固定
        maxmin: true,
        btn: ['确定', '清除', '取消'],
        yes: function (index, layero) {
            let datas = layero.find("iframe")[0].contentWindow.example.row('.success').data();
            let id = datas.id;
            let name = datas.name;

            $('#' + elementName + 'Id').val(id);
            $('#' + elementName + 'Name').val(name);

            let rownum = elementName.split('_')[0];
            initDetail(rownum, id);

            if ($("#form").data('bootstrapValidator')) {
                $("#form").data('bootstrapValidator').resetForm();
            }
            layer.close(index); //如果设定了yes回调，需进行手工关闭
        },
        cancel: function (index, layero) {
            if ($("#form").data('bootstrapValidator')) {
                $("#form").data('bootstrapValidator').resetForm();
            }
        },
        btn2: function (index, layero) {
            $('#' + elementName + 'Id').val('');
            $('#' + elementName + 'Name').val('');
            if ($("#form").data('bootstrapValidator')) {
                $("#form").data('bootstrapValidator').resetForm();
            }
        },
        btn3: function (index, layero) {
            if ($("#form").data('bootstrapValidator')) {
                $("#form").data('bootstrapValidator').resetForm();
            }
        }
    });
}

function openIFrame(url, title) {
    let index = layer.open({
        type: 2,
        title: title,
        shadeClose: true, //是否点击阴影关闭层
        area: ['800px', '400px'],
        offset: 'auto', //rb右下角弹出
        content: [url, 'yes'], //iframe的url，no代表不显示滚动条
        anim: 0,
        fixed: true, // 是否固定
        maxmin: true,
        end: function (index, layero) {
            if ('undefined' == typeof example) {
                location.reload();
            } else {
                // example.ajax.reload();
                example.draw(false);
            }
        }
    });
    layer.full(index);
}

/*
 初始化日期格式化
 调用例子：
 var time1 = new Date().Format("yyyy-MM-dd");
 var time2 = new Date().Format();
 var time3 = new Date().Format("yyyy-MM-dd HH:mm:ss");
  */
Date.prototype.Format = function (fmt) {
    if (!fmt) {
        fmt = 'yyyy-MM-dd';
    }
    const o = {
        "M+": this.getMonth() + 1, //月份
        "d+": this.getDate(), //日
        "H+": this.getHours(), //小时
        "m+": this.getMinutes(), //分
        "s+": this.getSeconds(), //秒
        "q+": Math.floor((this.getMonth() + 3) / 3), //季度
        "S": this.getMilliseconds() //毫秒
    };
    if (/(y+)/.test(fmt)) fmt = fmt.replace(RegExp.$1, (this.getFullYear() + "").substr(4 - RegExp.$1.length));
    for (let k in o) {
        if (new RegExp("(" + k + ")").test(fmt)) fmt = fmt.replace(RegExp.$1, (RegExp.$1.length == 1) ? (o[k]) : (("00" + o[k]).substr(("" + o[k]).length)));
    }
    return fmt;
};

//计算两个日期天数差
// 前面减后面，有正负
/**
 * @return {number}
 */
function DateDiff(sDate1, sDate2) {  //sDate1和sDate2是yyyy-MM-dd格式
    var time1 = Date.parse(new Date(sDate1));
    var time2 = Date.parse(new Date(sDate2));
    return parseInt((time1 - time2) / 1000 / 3600 / 24);
}


document.onkeydown = function (e) {
    var code = e.which || e.keyCode;
    if (code === 13) {
        $(".search").click();
    }
};

function getCurrentUserId() {
    return $("#currentUserId", window.top.document).val();
}

function getCurrentUserName() {
    return $("#currentUserName", window.top.document).val();
}

