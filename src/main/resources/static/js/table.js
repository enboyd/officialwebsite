function addItem(_data, displayFiledName, isMultiSelect) {
	var exist = $('#selectedItems').find('[data-id=' +_data['id'] + ']').length;
	if (!exist) {
		if (!isMultiSelect) {
			$('#selectedItems').empty();
		}

		let item = '<li><div style="border: 1px; overflow: hidden;">' +
			'<span style="width: 80%; display: inline-block; white-space: nowrap; overflow: hidden;" name="selectedItem" ';

		$.each(_data, function (k, v) {
			// item += 'data-'+k+'="' + JSON.stringify(v) + '" ';
            item += 'data-'+k+'="' + v + '" ';
		});

		item += '>' + _data[displayFiledName] + '&nbsp;</span>' +
			'<a style="width: 25px; float: right;" class="layui-btn layui-btn-xs" onclick="deleteItem(this)">' +
			'<i class="layui-icon layui-icon-close"></i></a></div></li>';
		$('#selectedItems').append(item);
	}
}

function deleteItem(_this) {
	$(_this).parents('li').remove();
}

$(function () {
	$('.deleteItem').on('click', function () {
		deleteItem(this);
	});
});