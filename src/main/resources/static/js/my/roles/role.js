function initRoles() {
    $.ajax({
        type: 'get',
        url: '/roles/all',
        async: false,
        success: function (data) {
                //拼接单选按钮
                for (let i = 0; i < data.length; i++) {
                    let d = data[i];
                    let id = d['id'];
                    let name = d['name'];
                    let r = $("#roles");

                    let t = "<label><input class='roleRadio' type='radio' value='" + id + "' name='roleId'>"
                        + name + "</label> &nbsp&nbsp";

                    r.append(t);
                }
        }
    });
}

function getCheckedRoleIds() {
    var ids = [];
    $(".roleRadio:checked").each(function () {
        ids.push($(this).val());
    });

    return ids;
}

function initRoleDatas(userId) {
    $.ajax({
        type: 'get',
        url: '/roles?userId=' + userId,
        success: function (data) {
            var length = data.length;
            for (var i = 0; i < length; i++) {
                $("input[type='checkbox']").each(function () {
                    var v = $(this).val();
                    if (v == data[i]['id']) {
                        $(this).attr("checked", true);
                    }
                });
            }
        }
    });
}