<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <link rel="stylesheet" type="text/css" media="screen" href="/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" media="screen" href="/css/dataTables.bootstrap.min.css">
    <link rel="stylesheet" type="text/css" media="screen" href="/layui/css/layui.css">
    <link rel="stylesheet" href="/css/treetable/jquery.treetable.css" />
    <link rel="stylesheet" href="/css/treetable/jquery.treetable.theme.default.css" />
    <script src="/js/libs/jquery-2.1.1.min.js"></script>
    <link rel="stylesheet" type="text/css" href="/css/bootstrap-select.min.css">
    <script src="/js/bootstrap/bootstrap.min.js"></script>
    <script src="/js/bootstrap/bootstrap-select.min.js"></script>
    <script src="/js/jq.js"></script>
    <script src="/js/plugin/bootstrapvalidator/bootstrapValidator.min.js"></script>
    <script src="/js/common.js"></script>
    <script src="/layui/layui.js"></script>
    <script src="/js/dict.js"></script>
    <script src="/js/libs/jquery.ztree.all-3.5.min.js"></script>
    <script src="/js/my/ztree-menu.js"></script>
    <script src="/js/my/permission.js"></script>
    <script src="/js/plugin/datatables/jquery.dataTables.min.js"></script>
    <script src="/js/plugin/datatables/dataTables.bootstrap.min.js"></script>
    <script src="/js/libs/jquery.treetable.js"></script>
</head>
<body>
<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 col-md-offset-2 col-lg-offset-2">
	<form class="form-horizontal" onsubmit="return false" id="form">
		<fieldset>
{addDivs}
			<div class="form-actions">
				<div class="row" align="center">
					<div class="col-md-12">
						<button class="btn btn-primary" type="submit" onclick="save()">
							<i class="fa fa-save"></i> 保存
						</button>
					</div>
				</div>
			</div>
		</fieldset>
	</form>
</div>
	<script type="text/javascript">
	    $(function () {
            $('#form').bootstrapValidator();
        });

		layui.use(['layer','laydate'], function(){
		    var layer = layui.layer;
		});
		
		function save() {
			$('#form').bootstrapValidator();
			var bootstrapValidator = $("#form").data('bootstrapValidator');
			bootstrapValidator.validate();
		    if(!bootstrapValidator.isValid()){
			   return;
		    }
		    
		    var formdata = $("#form").serializeObject();

			$.ajax({
				type : 'post',
				url : '/{beanParamName}/',
				contentType: "application/json; charset=utf-8",  
				data : JSON.stringify(formdata),
				success : function(data) {
					layer.msg("添加成功", {shift: -1, time: 1000}, function(){

                    });
				}
			});
		}
		
	</script>
</body>
</html>