package com.lty.jeeg.server.modules.sys.model;

import com.lty.jeeg.server.common.enums.CodeEnum;
import lombok.*;

/**
 * @author LTY
 */
@EqualsAndHashCode(callSuper = true)
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Notice extends BaseEntity<Long> {

	private static final long serialVersionUID = -4401913568806243090L;

	private String title;

	private String content;

	private Integer status;

	@Getter
	@NoArgsConstructor
	@AllArgsConstructor
	public enum Status implements CodeEnum<Integer> {
		/**
		 *
		 */
		DRAFT(0, "草稿"),
		PUBLISH(1, "发布");

		private Integer code;

		private String msg;
	}

	private String receiverId;

	private String receiverName;

}
