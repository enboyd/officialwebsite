package com.lty.jeeg.server.modules.sys.service.impl;

import com.lty.jeeg.server.common.constant.SysConstant;
import com.lty.jeeg.server.modules.sys.dao.NoticeDao;
import com.lty.jeeg.server.modules.sys.model.Notice;
import com.lty.jeeg.server.modules.sys.service.NoticeService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @author Administrator
 */
@Service
public class NoticeServiceImpl implements NoticeService {

    private final NoticeDao noticeDao;

    @Autowired
    public NoticeServiceImpl(NoticeDao noticeDao) {
        this.noticeDao = noticeDao;
    }

    @Override
    public void save(Notice notice) {
        noticeDao.save(notice);
        String receiverIds = notice.getReceiverId();
        if (StringUtils.isNotBlank(receiverIds)) {
            for (String userId: receiverIds.split(SysConstant.SEMICOLON_EN)) {
                noticeDao.saveReadRecord(notice.getId(), Long.parseLong(userId));
            }
        }
    }
}
