package com.lty.jeeg.server.modules.sys.model;

import java.util.Date;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.lty.jeeg.server.common.enums.CodeEnum;
import lombok.*;

/**
 * @author LTY
 */
@EqualsAndHashCode(callSuper = true)
@Data
public class User extends BaseEntity<Long> {

	private static final long serialVersionUID = -6525908145032868837L;

	private String username;

	private String password;

	@JsonIgnore
	private String salt;

	private String nickname;

	private List<Role> roleList;

	private List<Long> roleIds;

	private Dept dept;

	private String headImgUrl;

	private String phone;

	private String telephone;

	private String email;

	@JsonFormat(pattern = "yyyy-MM-dd")
	private Date birthday;

	private Integer sex;

	private Integer status;

	private Integer editable;

	@Getter
	@NoArgsConstructor
	@AllArgsConstructor
	public enum Status implements CodeEnum<Integer> {
		/**
		 *
		 */
		DISABLED(0, "离职"),
		VALID(1, "正常"),
		LOCKED(2, "锁定");

		private Integer code;

		private String msg;
	}

	public boolean isAdmin(){
		return isAdmin(this.id);
	}

	public static boolean isAdmin(Long id){
		return id != null && 1 == id;
	}

}
