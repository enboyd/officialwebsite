package com.lty.jeeg.server.modules.sys.model;

import com.lty.jeeg.server.modules.sys.model.BaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * @author : LTY
 * @date : 2019-04-16 15:24:31
 * Description : 状态流
 */
@EqualsAndHashCode(callSuper = true)
@Data
public class StatusFlow extends BaseEntity<Long> {

    private static final long serialVersionUID = 1L;

	/** 
	 类型
	  */
	private String type;

	/** 
	 key
	  */
	private String k;

	/** 
	 value
	  */
	private String val;

	/** 
	 上一项
	  */
	private Long prev;



}
