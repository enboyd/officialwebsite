package com.lty.jeeg.server.modules.sys.dao;

import java.util.List;

import com.lty.jeeg.server.modules.sys.model.Dept;
import org.apache.ibatis.annotations.*;

import org.springframework.stereotype.Repository;

/**
 * @author : LTY
 * @date : 2018-12-05 09:42:50
 * Description :
 */
@Mapper
@Repository
public interface DeptDao {

    /**
     * 根据部门ID查找部门
     * @param id 部门ID
     * @return Dept
     */
    @Select("select d1.*, d2.name parentName, u1.nickname primaryUserName, u2.nickname deputyUserName " +
            "from sys_dept d1 " +
            "left join sys_dept d2 on d1.parentId = d2.id " +
            "left join sys_user u1 on d1.primaryUserId = u1.id " +
            "left join sys_user u2 on d1.deputyUserId = u2.id " +
            "where d1.id = #{id}")
    Dept getById(Long id);

    /**
     * 获取上级
     * @param id ID
     * @return 上级bean
     */
    Dept getParentById(Long id);

    /**
     * 慎用！！！
     * 根据部门ID删除部门
     * @param id 部门ID
     * @return 1/0
     */
    @Delete("delete from sys_dept where id = #{id}")
    int delete(Long id);

    /**
     * 更新
     * @param dept 部门实体
     * @return 1/0
     */
    int update(Dept dept);

    /**
     * 保存
     * @param dept 部门实体
     * @return 1/0
     */
    @Options(useGeneratedKeys = true)
    @Insert("insert into sys_dept(parentId, parentIds, name, type, level, primaryUserId, deputyUserId, sort, remark) " +
            "values(#{parentId}, #{parentIds}, #{name}, #{type}, #{level}, #{primaryUserId}, #{deputyUserId}, #{sort}, #{remark})")
    Long save(Dept dept);

    /**
     * 部门树状列表
     * for 列表页面
     * @return 列表
     */
    @Select("select " +
            "d.*, u1.nickname primaryUserName, u2.nickname deputyUserName " +
            "from " +
            "sys_dept d " +
            "left join sys_user u1 on d.primaryUserId = u1.id " +
            "left join sys_user u2 on d.deputyUserId = u2.id " +
            "order by d.sort")
    List<Dept> treeList();

    /**
     * 部门树状列表
     * for Ztree
     * @return 所有数据
     */
    @Select("select d.* from sys_dept d where d.isPublic = 1 order by d.sort")
    List<Dept> listAll();

}
