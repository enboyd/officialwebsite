package com.lty.jeeg.server.modules.sys.controller;

import java.util.List;

import com.lty.jeeg.server.common.dto.BeanField;
import com.lty.jeeg.server.common.dto.GenerateDetail;
import com.lty.jeeg.server.common.dto.GenerateInput;
import com.lty.jeeg.server.common.dto.ResponseInfo;
import com.lty.jeeg.server.common.utils.ResponseUtil;
import com.lty.jeeg.server.modules.sys.service.GenerateService;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.lty.jeeg.server.common.annotation.LogAnnotation;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

/**
 * 代码生成接口
 * 
 * @author 小威老师 xiaoweijiagou@163.com
 *
 */
@Api(tags = "代码生成")
@RestController
@RequestMapping("/generate")
public class GenerateController {

	@Autowired
	private GenerateService generateService;

	@ApiOperation("获取所有表名")
	@GetMapping(value = "/tables")
	@RequiresPermissions("generate:edit")
	public ResponseInfo getTableNames() {
		return ResponseUtil.success(generateService.getTableNames());
	}

	@ApiOperation("根据表名显示表信息")
	@GetMapping(params = { "tableName" })
	@RequiresPermissions("generate:edit")
	public GenerateDetail generateByTableName(String tableName) {
		GenerateDetail detail = new GenerateDetail();
		detail.setBeanName(generateService.upperFirstChar(tableName));
		List<BeanField> fields = generateService.listBeanField(tableName);
		detail.setFields(fields);

		return detail;
	}

	@LogAnnotation
	@ApiOperation("生成代码")
	@PostMapping
	@RequiresPermissions("generate:edit")
	public void save(@RequestBody GenerateInput input) {
		generateService.saveCode(input);
	}

}
