package com.lty.jeeg.server.modules.sys.model;

import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.List;

/**
 * @author LTY
 */
@EqualsAndHashCode(callSuper = true)
@Data
public class Permission extends BaseEntity<Long> {

	private static final long serialVersionUID = 6180869216498363919L;

	private Long parentId;

	private String name;

	private String css;

	private String href;

	private Integer type;

	private String permission;

	private Integer sort;

	private Integer editable;

	private List<Permission> child;

}
