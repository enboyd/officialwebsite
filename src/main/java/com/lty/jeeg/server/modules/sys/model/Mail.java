package com.lty.jeeg.server.modules.sys.model;

import com.lty.jeeg.server.common.enums.CodeEnum;
import lombok.*;

/**
 * @author LTY
 */
@EqualsAndHashCode(callSuper = true)
@Data
public class Mail extends BaseEntity<Long> {

	private static final long serialVersionUID = 5613231124043303948L;

	private Long userId;

	private String toUsers;

	private String subject;

	private String content;

	private Integer status;

	@Getter
	@NoArgsConstructor
	@AllArgsConstructor
	public enum Status implements CodeEnum<Integer> {
		/**
		 *
		 */
		SUCCESS(0, "成功"),
		FAIL(1, "失败");

		private Integer code;

		private String msg;
	}
}
