package com.lty.jeeg.server.modules.sys.service.impl;

import com.lty.jeeg.server.modules.sys.dao.PermissionDao;
import com.lty.jeeg.server.modules.sys.model.Permission;
import com.lty.jeeg.server.modules.sys.service.PermissionService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * @author LTY
 */
@Service
public class PermissionServiceImpl implements PermissionService {

	private static final Logger log = LoggerFactory.getLogger("adminLogger");

	private final PermissionDao permissionDao;

	@Autowired
	public PermissionServiceImpl(PermissionDao permissionDao) {
		this.permissionDao = permissionDao;
	}

	@Override
	public void save(Permission permission) {
		permissionDao.save(permission);
		permissionDao.addRolePermission(1L, permission.getId());
		log.debug("新增菜单{}", permission.getName());
	}

	@Override
	public void update(Permission permission) {
		permissionDao.update(permission);
	}

	@Override
	@Transactional(rollbackFor = RuntimeException.class)
	public void delete(Long id) {
		permissionDao.deleteRolePermission(id);
		permissionDao.delete(id);
		permissionDao.deleteByParentId(id);

		log.debug("删除菜单id:{}", id);
	}

}
