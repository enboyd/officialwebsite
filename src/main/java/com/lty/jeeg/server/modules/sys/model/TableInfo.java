package com.lty.jeeg.server.modules.sys.model;


import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * @author LTY
 */
@EqualsAndHashCode(callSuper = true)
@Data
public class TableInfo extends BaseEntity<Long> {

    private static final long serialVersionUID = 6841478894242628239L;

    private String tableName;

    private String tableComment;
}
