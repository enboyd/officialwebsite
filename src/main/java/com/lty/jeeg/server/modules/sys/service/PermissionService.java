package com.lty.jeeg.server.modules.sys.service;

import com.lty.jeeg.server.modules.sys.model.Permission;

/**
 * @author LTY
 */
public interface PermissionService {

	void save(Permission permission);

	void update(Permission permission);

	void delete(Long id);
}
