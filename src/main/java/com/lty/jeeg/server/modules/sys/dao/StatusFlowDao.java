package com.lty.jeeg.server.modules.sys.dao;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Options;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import org.springframework.stereotype.Repository;

import com.lty.jeeg.server.modules.sys.model.StatusFlow;

/**
 * @author : LTY
 * @date : 2019-04-16 15:24:31
 * Description : 状态流
 */
@Mapper
@Repository
public interface StatusFlowDao {

    /**
     * 根据ID获取
     * @param id ID
     * @return bean
     */
    @Select("select * from t_status_flow t where t.id = #{id}")
    StatusFlow getById(Long id);

    /**
     * 删除
     * @param id ID
     * @return 1/0
     */
    @Delete("delete from t_status_flow where id = #{id}")
    int delete(Long id);

    /**
     * 修改
     * @param statusFlow ID
     * @return 1/0
     */
    int update(StatusFlow statusFlow);

    /**
     * 保存
     * @param statusFlow bean
     * @return ID
     */
    @Options(useGeneratedKeys = true, keyProperty = "id")
    @Insert("insert into t_status_flow(type, k, val, prev) values(#{type}, #{k}, #{val}, #{prev})")
    Long save(StatusFlow statusFlow);

    /**
     * 计数
     * @param params 筛选条件
     * @return 总数
     */
    int count(@Param("params") Map<String, Object> params);

    /**
     * 列表
     * @param params 筛选条件
     * @param offset 页码
     * @param limit 分页数目
     * @return beanList
     */
    List<StatusFlow> list(@Param("params") Map<String, Object> params, @Param("offset") Integer offset, @Param("limit") Integer limit);

    List<StatusFlow> getListByTypeAndK(@Param("type") String type, @Param("prev") String prev);
}
