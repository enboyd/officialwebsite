package com.lty.jeeg.server.modules.sys.model;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * @author LTY
 */
@EqualsAndHashCode(callSuper = true)
@Data
public class SysLogs extends BaseEntity<Long> {

	private static final long serialVersionUID = -7809315432127036583L;

	private User user;

	private String module;

	private Boolean flag;

	private String remark;

}
