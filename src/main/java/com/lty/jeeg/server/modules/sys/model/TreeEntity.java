package com.lty.jeeg.server.modules.sys.model;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * @author administered
 */
@EqualsAndHashCode(callSuper = true)
@Data
public class TreeEntity extends BaseEntity<Long> {

    private static final long serialVersionUID = -962565780643817344L;

    private Long parentId;

    private String parentIds;

    private Integer sort;
}
