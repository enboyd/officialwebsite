package com.lty.jeeg.server.modules.sys.dao;

import java.util.List;
import java.util.Map;

import com.lty.jeeg.server.common.dto.UserDto;
import com.lty.jeeg.server.common.vo.UserVO;
import com.lty.jeeg.server.modules.sys.model.User;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Options;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

import org.springframework.stereotype.Repository;

/**
 * @author LTY
 */
@Mapper
@Repository
public interface UserDao {

    @Options(useGeneratedKeys = true, keyProperty = "id")
    @Insert("insert into sys_user(username, password, salt, nickname, deptId, headImgUrl, phone, telephone, email, birthday, sex, status, createBy, updateBy) values(#{username}, #{password}, #{salt}, #{nickname}, #{deptId}, #{headImgUrl}, #{phone}, #{telephone}, #{email}, #{birthday}, #{sex}, #{status}, #{createBy}, #{updateBy})")
    int save(User user);

//    @Select("select t.*, r.id 'role.id', r.name 'role.name', r.dataScope 'role.dataScope', d.id 'dept.id', d.name 'dept.name', d.parentId 'dept.parentId', d.parentIds 'dept.parentIds', d.company 'dept.company' from sys_user t left join sys_dept d on t.deptId = d.id left join sys_role r on t.role = r.id where t.id = #{id}")
//    User getById(Long id);

    @Select("select t.*, d.id 'dept.id', d.name 'dept.name', d.parentId 'dept.parentId', d.parentIds 'dept.parentIds', d.company 'dept.company' from sys_user t left join sys_dept d on t.deptId = d.id where t.id = #{id}")
    User getById(Long id);

    @Select("select t.*, d.id 'dept.id', d.name 'dept.name', d.parentId 'dept.parentId', d.parentIds 'dept.parentIds', d.company 'dept.company' from sys_user t left join sys_dept d on t.deptId = d.id where t.username = #{username}")
    User getUser(String username);

    @Update("update sys_user t set t.password = #{password} where t.id = #{id}")
    int changePassword(@Param("id") Long id, @Param("password") String password);

    Integer count(@Param("params") Map<String, Object> params);

    List<UserDto> list(@Param("params") Map<String, Object> params, @Param("offset") Integer offset,
                       @Param("limit") Integer limit);

    @Delete("delete from sys_role_user where userId = #{userId}")
    void deleteUserRole(Long userId);

    int saveUserRoles(@Param("userId") Long userId, @Param("roleIds") List<Long> roleIds);

    /**
     * @param user 用户
     * @return
     */
    int update(User user);

    /**
     * @param id 用户ID
     */
    @Update("update sys_user set isDeleted = 1 where id = #{id}")
    void delete(Long id);

    /**
     * 根据ID查询该部门及以下部门中是否有用户
     *
     * @param id 用户ID
     * @return 结果总数
     */
    @Select("select count(1) from sys_user u " +
            "LEFT JOIN sys_dept d on u.deptId = d.id " +
            "where u.isDeleted = 0 and (u.deptId = #{id} " +
            "or d.parentId =#{id})")
    Integer countUser(Long id);

    /**
     * 返回全部用户
     * 供At.js使用
     * @return
     */
    List<UserVO> listAll();

    /**
     * 返回部分用户
     * @return
     */
    List<UserVO> partList(Long id);
}
