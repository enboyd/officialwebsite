package com.lty.jeeg.server.modules.sys.model;

import com.lty.jeeg.server.common.enums.CodeEnum;
import lombok.*;

/**
 * @author LTY
 */
@EqualsAndHashCode(callSuper = true)
@Data
public class Role extends BaseEntity<Long> {

	private static final long serialVersionUID = -3802292814767103648L;

	private String name;

	private String description;

	private String remark;

	private Integer dataScope;

	private Integer usable;

	private Integer editable;

	/**
	 * 数据范围
	 */
	@Getter
	@NoArgsConstructor
	@AllArgsConstructor
	public enum dataScope implements CodeEnum<Integer> {
		/**
		 *
		 */
		DATA_SCOPE_ALL(1, "所有数据"),
		DATA_SCOPE_OFFICE_AND_CHILD(4, "所在部门及以下数据"),
		DATA_SCOPE_OFFICE(5, "所在部门数据"),
		DATA_SCOPE_SELF(8, "仅本人数据"),
 		;

		private Integer code;

		private String msg;
	}

}
