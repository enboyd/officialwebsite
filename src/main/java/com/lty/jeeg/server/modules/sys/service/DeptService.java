package com.lty.jeeg.server.modules.sys.service;

import com.lty.jeeg.server.common.enums.ResponseEnum;
import com.lty.jeeg.server.modules.sys.model.Dept;

import java.util.List;

/**
 * @author : LTY
 * @date : 2018-12-05 09:42:50
 * Description :
 */
public interface DeptService {

    Dept getById(Long id);

    Dept getParentById(Long id);

    ResponseEnum delete(Long id);

    int update(Dept dept);

    Long save(Dept dept);

    List<Dept> treeList();

    List<Dept> listAll();

}
