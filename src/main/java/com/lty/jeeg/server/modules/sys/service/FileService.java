package com.lty.jeeg.server.modules.sys.service;

import java.io.IOException;
import java.util.List;
import java.util.Map;

import com.lty.jeeg.server.modules.sys.model.FileInfo;
import org.springframework.web.multipart.MultipartFile;

/**
 * @author LTY
 */
public interface FileService {

	FileInfo save(MultipartFile file) throws IOException;

	void delete(String id);

    int count(Map<String, Object> params);

	List<FileInfo> list(Map<String, Object> params, Integer offset, Integer limit);

    FileInfo getById(String id);
}
