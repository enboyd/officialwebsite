package com.lty.jeeg.server.modules.sys.model;

import com.google.common.collect.Maps;
import com.lty.jeeg.server.common.annotation.FieldName;
import com.lty.jeeg.server.common.utils.UserUtil;
import com.lty.jeeg.server.common.enums.CodeEnum;
import lombok.*;

import java.io.Serializable;
import java.util.Date;
import java.util.Map;

/**
 * @author LTY
 */
@Data
public abstract class BaseEntity<ID extends Serializable> implements Serializable {

	private static final long serialVersionUID = 2054813493011812469L;

	@FieldName(value = "ID")
	protected ID id;

	@FieldName(value = "备注")
	private String remark;

	@FieldName(value = "创建时间")
	private Date createTime;

	@FieldName(value = "最后修改时间")
	private Date updateTime;

	@FieldName(value = "创建人")
	private long createBy;

	@FieldName(value = "最后修改人")
	private long updateBy;

	@FieldName(value = "是否已删除")
	private Integer isDeleted = 0;

	/**
	 * 自定义SQL（SQL标识，SQL内容）
	 */
	protected Map<String, String> sqlMap;

	public Map<String, String> getSqlMap() {
		if (sqlMap == null) {
			sqlMap = Maps.newHashMap();
		}
		return sqlMap;
	}

	@Getter
	@NoArgsConstructor
	@AllArgsConstructor
	public enum IsDeleted implements CodeEnum<Integer> {
		/**
		 *
		 */
		NORMAL(0, "正常"),
		DELETED(1, "删除");

		private Integer code;

		private String msg;
	}

	public void preInsert() {
		User user = UserUtil.getCurrentUser();
		if (user.getId() != null) {
			this.createBy = user.getId();
			this.updateBy = user.getId();
		}
	}

	public void preUpdate() {
		User user = UserUtil.getCurrentUser();
		if (user.getId() != null) {
			this.updateBy = user.getId();
		}
	}
}
