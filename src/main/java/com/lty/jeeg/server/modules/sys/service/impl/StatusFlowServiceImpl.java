package com.lty.jeeg.server.modules.sys.service.impl;

import com.lty.jeeg.server.common.page.table.PageTableHandler;
import com.lty.jeeg.server.common.page.table.PageTableRequest;
import com.lty.jeeg.server.common.page.table.PageTableResponse;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.lty.jeeg.server.modules.sys.dao.StatusFlowDao;
import com.lty.jeeg.server.modules.sys.model.StatusFlow;
import com.lty.jeeg.server.modules.sys.service.StatusFlowService;

import java.util.List;

/**
 * @author : LTY
 * @date : 2019-04-16 15:24:31
 * Description : 状态流
 */
@Service
@Slf4j
public class StatusFlowServiceImpl implements StatusFlowService {

    private final StatusFlowDao statusFlowDao;

    @Autowired
    public StatusFlowServiceImpl(StatusFlowDao statusFlowDao) {
        this.statusFlowDao = statusFlowDao;
    }

    @Override
    public StatusFlow getById(Long id) {
        return statusFlowDao.getById(id);
    }

    @Override
	@Transactional(rollbackFor = Exception.class)
	public int delete(Long id) {
		log.debug("删除StatusFlow=>{}", id);
		return statusFlowDao.delete(id);
	}

    @Override
	@Transactional(rollbackFor = Exception.class)
	public int update(StatusFlow statusFlow) {
	    statusFlow.preUpdate();
        log.debug("修改StatusFlow=>{}", statusFlow.getId());
        return statusFlowDao.update(statusFlow);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public Long save(StatusFlow statusFlow) {
        statusFlow.preInsert();
        statusFlowDao.save(statusFlow);
        log.debug("新增StatusFlow=>{}", statusFlow.getId());
        return statusFlow.getId();
    }

    @Override
    public PageTableResponse list(PageTableRequest request) {
        return new PageTableHandler(
                request1 -> statusFlowDao.count(request1.getParams()),
                request2 -> statusFlowDao.list(
                        request2.getParams(), request2.getOffset(), request2.getLimit()))
                .handle(request);
    }

    @Override
    public List<StatusFlow> getListByTypeAndK(String type, String prev) {
        return statusFlowDao.getListByTypeAndK(type, prev);
    }


}
