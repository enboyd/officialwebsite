package com.lty.jeeg.server.modules.sys.service.impl;

import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import com.lty.jeeg.server.common.utils.UserUtil;
import com.lty.jeeg.server.modules.sys.dao.FileInfoDao;
import com.lty.jeeg.server.modules.sys.model.FileInfo;
import com.lty.jeeg.server.common.utils.FileUtil;
import com.lty.jeeg.server.modules.sys.model.User;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import com.lty.jeeg.server.modules.sys.service.FileService;

/**
 * @author LTY
 */
@Service
public class FileServiceImpl implements FileService {
	
	private static final Logger log = LoggerFactory.getLogger("adminLogger");

	@Value("${files.path}")
	private String filesPath;
	@Autowired
	private FileInfoDao fileInfoDao;

	@Override
	public FileInfo save(MultipartFile file) throws IOException {
		String fileOrigName = file.getOriginalFilename();
		if (!fileOrigName.contains(".")) {
			throw new IllegalArgumentException("缺少后缀名");
		}
		String fileName = file.getOriginalFilename();	//文件名称

		String md5 = FileUtil.fileMd5(file.getInputStream());

		fileOrigName = fileOrigName.substring(fileOrigName.lastIndexOf("."));
		String pathname = FileUtil.getPath() + md5 + fileOrigName;
		String fullPath = filesPath +"/"+UserUtil.getCurrentUser().getId()+ pathname;
		FileUtil.saveFile(file, fullPath);

		long size = file.getSize();
		String contentType = file.getContentType();

		FileInfo fileInfo = new FileInfo();

		fileInfo.setId(UUID.randomUUID().toString());
		fileInfo.setContentType(contentType);
		fileInfo.setSize(size);
		fileInfo.setPath(fullPath);
		fileInfo.setUrl("/"+UserUtil.getCurrentUser().getId()+ pathname);
		fileInfo.setType(contentType.startsWith("image/") ? 1 : 0);
		fileInfo.setFileName(fileName);

		fileInfoDao.save(fileInfo);

		log.debug("上传文件{}", fullPath);

		return fileInfo;

	}

	@Override
	public void delete(String id) {
		FileInfo fileInfo = fileInfoDao.getById(id);
		if (fileInfo != null) {
			String fullPath = fileInfo.getPath();
			FileUtil.deleteFile(fullPath);

			fileInfoDao.delete(id);
			log.debug("删除文件：{}", fileInfo.getPath());
		}
	}

	@Override
	public int count(Map<String, Object> params) {
		params.put("urlLike", "/" + UserUtil.getCurrentUser().getId());
		return fileInfoDao.count(params);
	}

	@Override
	public List<FileInfo> list(Map<String, Object> params, Integer offset, Integer limit) {
		params.put("urlLike", "/" + UserUtil.getCurrentUser().getId());
		return fileInfoDao.list(params, offset, limit);
	}

	@Override
	public FileInfo getById(String id) {
		return fileInfoDao.getById(id);
	}


}
