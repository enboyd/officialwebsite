package com.lty.jeeg.server.modules.sys.service;

import com.alibaba.fastjson.JSONArray;
import com.lty.jeeg.server.common.dto.UserDto;
import com.lty.jeeg.server.modules.sys.model.User;

import java.util.List;

/**
 * @author LTY
 */
public interface UserService {

	User saveUser(UserDto userDto);
	
	User updateUser(UserDto userDto);

	String passwordEncoder(String credentials, String salt);

	User getUser(String username);

	void changePassword(String username, String oldPassword, String newPassword);

	void delete(Long id);

    JSONArray listAll();

	JSONArray partList(Long id);

    User currentUser();
}
