package com.lty.jeeg.server.modules.sys.controller;

import com.lty.jeeg.server.common.dto.ResponseInfo;
import com.lty.jeeg.server.common.utils.ResponseUtil;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.lty.jeeg.server.common.page.table.PageTableRequest;
import com.lty.jeeg.server.common.page.table.PageTableResponse;

import com.lty.jeeg.server.modules.sys.model.StatusFlow;
import com.lty.jeeg.server.modules.sys.service.StatusFlowService;

import io.swagger.annotations.ApiOperation;

/**
 * @author : LTY
 * @date : 2019-04-16 15:24:31
 * Description : 状态流
 */
@Api(tags = "状态流")
@RestController
@RequestMapping("/statusFlow")
public class StatusFlowController {

    private final StatusFlowService statusFlowService;

    @Autowired
    public StatusFlowController(StatusFlowService statusFlowService) {
        this.statusFlowService = statusFlowService;
    }

    @GetMapping("/{id}")
    @ApiOperation(value = "根据id获取")
    public ResponseInfo get(@PathVariable Long id) {
        return ResponseUtil.success(statusFlowService.getById(id));
    }

    @PostMapping
    @ApiOperation(value = "保存")
    public ResponseInfo save(@RequestBody StatusFlow statusFlow) {
        return ResponseUtil.success(statusFlowService.save(statusFlow));
    }

    @PutMapping
    @ApiOperation(value = "修改")
    public ResponseInfo update(@RequestBody StatusFlow statusFlow) {
        return ResponseUtil.success(statusFlowService.update(statusFlow));
    }

    @DeleteMapping("/{id}")
    @ApiOperation(value = "删除")
    public ResponseInfo delete(@PathVariable Long id) {
        return ResponseUtil.success(statusFlowService.delete(id));
    }

    @GetMapping("/list")
    @ApiOperation(value = "列表")
    public PageTableResponse list(PageTableRequest request) {
        return statusFlowService.list(request);
    }

    @GetMapping("/list/{type}/{prev}")
    @ApiOperation(value = "根据type和prev获取列表")
    public ResponseInfo getListByTypeAndK(@PathVariable(value = "type") String type, @PathVariable(value = "prev") String prev) {
        return ResponseUtil.success(statusFlowService.getListByTypeAndK(type, prev));
    }

}
