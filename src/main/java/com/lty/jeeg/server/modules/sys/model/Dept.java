package com.lty.jeeg.server.modules.sys.model;


import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * @author LTY
 * @date : 2018-12-05 09:42:50
 * Description :
 */
@EqualsAndHashCode(callSuper = true)
@Data
public class Dept extends TreeEntity {

	private static final long serialVersionUID = -4494474167197546092L;

	private String name;

	private String parentName;

	private Integer type;

	private Integer level;

	private Integer primaryUserId;

	private String primaryUserName;

	private Integer deputyUserId;

	private String deputyUserName;

	private Integer editable;

}
