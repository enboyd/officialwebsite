package com.lty.jeeg.server.modules.sys.model;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * @author LTY
 */
@EqualsAndHashCode(callSuper = true)
@Data
public class Dict extends BaseEntity<Long> {

	private static final long serialVersionUID = -2431140186410912787L;

	private String type;
	private String k;
	private String val;
	private Integer editable;

}
