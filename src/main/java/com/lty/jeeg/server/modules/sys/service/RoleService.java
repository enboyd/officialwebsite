package com.lty.jeeg.server.modules.sys.service;

import com.lty.jeeg.server.common.dto.RoleDto;
import com.lty.jeeg.server.modules.sys.model.Role;
import com.lty.jeeg.server.modules.sys.model.User;

/**
 * @author LTY
 */
public interface RoleService {

    void saveRole(RoleDto roleDto);

	void deleteRole(Long id);
}
