package com.lty.jeeg.server.modules.sys.service;

import java.util.List;

import javax.mail.MessagingException;

/**
 * @author LTY
 */
public interface SendMailSevice {

	/**
	 * 
	 * @param toUser 接收人
	 * @param subject 标题
	 * @param text 内容（支持html格式）
	 */
	void sendMail(List<String> toUser, String subject, String text);

	void sendMail(String toUser, String subject, String text) throws MessagingException;
}
