package com.lty.jeeg.server.modules.sys.service;

import com.lty.jeeg.server.modules.sys.model.StatusFlow;
import com.lty.jeeg.server.common.page.table.PageTableRequest;
import com.lty.jeeg.server.common.page.table.PageTableResponse;

import java.util.List;

/**
 * @author : LTY
 * @date : 2019-04-16 15:24:31
 * Description : 状态流
 */
public interface StatusFlowService {

    /**
     * 根据ID获取
     * @param id ID
     * @return bean
     */
    StatusFlow getById(Long id);

    /**
     * 删除
     * @param id ID
     * @return 1/0
     */
    int delete(Long id);

    /**
     * 修改
     * @param statusFlow bean
     * @return 1/0
     */
    int update(StatusFlow statusFlow);

    /**
     * 保存
     * @param statusFlow bean
     * @return ID
     */
    Long save(StatusFlow statusFlow);

    /**
     * 列表
     * @param request ID
     * @return 分页
     */
    PageTableResponse list(PageTableRequest request);

    List<StatusFlow> getListByTypeAndK(String type, String prev);
}
