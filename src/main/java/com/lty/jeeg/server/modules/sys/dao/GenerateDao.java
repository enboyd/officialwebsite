package com.lty.jeeg.server.modules.sys.dao;

import com.lty.jeeg.server.modules.sys.model.TableInfo;
import org.apache.ibatis.annotations.*;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @author : LTY
 * @date : 2018-12-05 09:42:50
 * Description :
 */
@Mapper
@Repository
public interface GenerateDao {

    /**
     * 获得所有表名
     * @return 列表
     */
    @Select("select table_name tableName, table_comment tableComment from information_schema.tables where table_schema = (select database())")
    List<TableInfo> getTableNames();

}
