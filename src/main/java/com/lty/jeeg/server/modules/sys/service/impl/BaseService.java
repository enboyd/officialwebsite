package com.lty.jeeg.server.modules.sys.service.impl;

import com.lty.jeeg.server.common.constant.SysConstant;
import com.lty.jeeg.server.modules.sys.model.Role;
import com.lty.jeeg.server.modules.sys.model.User;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;


/**
 * @author LTY
 */
@Transactional(readOnly = true, rollbackFor = RuntimeException.class)
@Slf4j
public abstract class BaseService {


    /**
     * 数据范围过滤
     *
     * @param user      当前用户对象
     * @param deptAlias 机构表别名，多个用“,”逗号隔开。
     * @param userAlias 用户表别名，多个用“,”逗号隔开，传递空，忽略此参数
     * @return 标准连接条件对象
     */
    public static String dataScopeFilter(User user, String deptAlias, String userAlias) {
        StringBuilder sqlString = new StringBuilder();
        // 超级管理员，跳过权限过滤
        if (!user.isAdmin()) {
            boolean isDataScopeAll = false;
            Role r = user.getRoleList().get(0);
            if(r != null && r.getId() != null){
                for (String oa : StringUtils.split(deptAlias, SysConstant.COMMA_EN)) {
                    if (StringUtils.isNotBlank(oa)) {
                        if (Role.dataScope.DATA_SCOPE_ALL.getCode().equals(r.getDataScope())) {
                            isDataScopeAll = true;
                        } else if (Role.dataScope.DATA_SCOPE_OFFICE_AND_CHILD.getCode().equals(r.getDataScope())) {
                            sqlString.append(" OR ").append(oa).append(".id = '").append(user.getDept().getId()).append("'");
                            sqlString.append(" OR ").append(oa).append(".parentIds LIKE '").append(user.getDept().getParentIds()).append(user.getDept().getId()).append(",%'");
                        } else if (Role.dataScope.DATA_SCOPE_OFFICE.getCode().equals(r.getDataScope())) {
                            sqlString.append(" OR ").append(oa).append(".id = '").append(user.getDept().getId()).append("'");
                        }
                    }
                }
                // 如果没有全部数据权限，并设置了用户别名，则当前权限为本人；如果未设置别名，当前无权限为已植入权限
                if (!isDataScopeAll) {
                    if (StringUtils.isNotBlank(userAlias)) {
                        for (String ua : StringUtils.split(userAlias, SysConstant.COMMA_EN)) {
                            sqlString.append(" OR ").append(ua).append(".id = '").append(user.getId()).append("'");
                        }
                    }
                } else {
                    // 如果包含全部权限，则去掉之前添加的所有条件，并跳出循环。
                    sqlString = new StringBuilder();
                }
            }
        }
        if (StringUtils.isNotBlank(sqlString.toString())) {
            return " AND (" + sqlString.substring(4) + ")";
        }
        return "";
    }

}
