package com.lty.jeeg.server.modules.sys.service;

import org.quartz.JobDataMap;
import org.quartz.SchedulerException;

import com.lty.jeeg.server.modules.sys.model.JobModel;

/**
 * @author LTY
 */
public interface JobService {

	void saveJob(JobModel jobModel);

	void doJob(JobDataMap jobDataMap);

	void deleteJob(Long id) throws SchedulerException;
}
