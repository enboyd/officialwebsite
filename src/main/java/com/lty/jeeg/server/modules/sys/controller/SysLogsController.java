package com.lty.jeeg.server.modules.sys.controller;

import java.util.List;

import com.lty.jeeg.server.modules.sys.dao.SysLogsDao;
import com.lty.jeeg.server.modules.sys.model.SysLogs;
import com.lty.jeeg.server.common.page.table.PageTableHandler;
import com.lty.jeeg.server.common.page.table.PageTableRequest;
import com.lty.jeeg.server.common.page.table.PageTableResponse;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

/**
 * @author LTY
 */
@Api(tags = "日志")
@RestController
@RequestMapping("/logs")
public class SysLogsController {

	private final SysLogsDao sysLogsDao;

	@Autowired
	public SysLogsController(SysLogsDao sysLogsDao) {
		this.sysLogsDao = sysLogsDao;
	}

	@GetMapping
	@RequiresPermissions(value = "sys:log:query")
	@ApiOperation(value = "日志列表")
	public PageTableResponse list(PageTableRequest request) {
		return new PageTableHandler(new PageTableHandler.CountHandler() {

			@Override
			public int count(PageTableRequest request) {
				return sysLogsDao.count(request.getParams());
			}
		}, new PageTableHandler.ListHandler() {

			@Override
			public List<SysLogs> list(PageTableRequest request) {
				return sysLogsDao.list(request.getParams(), request.getOffset(), request.getLimit());
			}
		}).handle(request);
	}

}
