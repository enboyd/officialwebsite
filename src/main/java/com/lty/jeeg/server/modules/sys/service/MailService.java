package com.lty.jeeg.server.modules.sys.service;

import java.util.List;

import com.lty.jeeg.server.modules.sys.model.Mail;

/**
 * @author LTY
 */
public interface MailService {

	void save(Mail mail, List<String> toUser);
}
