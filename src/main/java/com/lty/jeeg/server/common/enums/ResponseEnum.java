package com.lty.jeeg.server.common.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

/**
 * @author LTY
 */

@Getter
@NoArgsConstructor
@AllArgsConstructor
public enum ResponseEnum {
    /**
     *
     */

    SUCCESS(0, "成功"),
    ERROR(1, "异常"),

    LOGIN_SUCCESS(10, "登录成功"),
    LOGIN_FAIL(11, "登录失败"),
    LOGIN_FAIL_UNAUTHORIZED_OPENID(12, "登录失败，未授权的微信号"),
    LOGIN_FAIL_UNREGISTERED_ACCOUNT(13, "登录失败，未注册的账号"),
    LOGIN_FAIL_WRONG_PASSWORD(14, "登录失败，登录密码错误"),
    API_AUTH_FAIL(15, "接口授权认证失败"),

    LOGOUT_SUCCESS(20, "登出成功"),

    ORDER_NOT_EXIST(92, "订单不存在"),
    ORDER_DETAILS_NOT_EXIST(93, "订单明细不存在"),
    ORDER_STATUS_ERROR(94, "订单状态不符"),
    ORDER_UPDATE_FAIL(95, "订单更新失败"),
    ORDER_PAY_STATUS_ERROR(94, "订单状态错误"),
    ORDER_AMOUNT_NOT_EQUAL(95, "订单金额不一致"),

    FORM_INFO_INCOMPLETE(101, "表单信息不全"),
    APPROVAL_STATUS_CANNOT_MODIFY(102, "提交审批后不可修改"),


    DEPT_NOT_FOUND(112, "无此部门"),
    DEPT_USER_EXIST_ERROR(113, "请删除部门下所有用户后再删除该部门"),

    HAND_OVER_DEPTS_SHOULD_NOT_SAME(121, "来源部门不能与需求部门一致"),
    STOCK_IDLE_NUMBER_ERROR(122, "闲置数量不能大于库存总数量"),

    ;
    /**
     * 自定义返回码
     */
    private Integer code;

    /**
     * 返回信息
     */
    private String msg;
}
