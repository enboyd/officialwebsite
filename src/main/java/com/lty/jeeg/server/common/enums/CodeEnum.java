package com.lty.jeeg.server.common.enums;

/**
 * @author LTY
 */
public interface CodeEnum<T> {

    /**
     * 获取返回码
     * @return
     */
    T getCode();
}