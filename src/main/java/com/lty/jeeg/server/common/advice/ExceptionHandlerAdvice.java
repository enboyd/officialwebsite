package com.lty.jeeg.server.common.advice;

import com.lty.jeeg.server.common.exception.MyException;
import com.lty.jeeg.server.common.utils.ResponseUtil;
import org.apache.shiro.authc.IncorrectCredentialsException;
import org.apache.shiro.authc.UnknownAccountException;
import org.apache.shiro.authz.UnauthorizedException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.web.bind.MissingServletRequestParameterException;
import org.springframework.web.bind.UnsatisfiedServletRequestParameterException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.method.annotation.MethodArgumentTypeMismatchException;

import com.lty.jeeg.server.common.dto.ResponseInfo;

/**
 * springmvc异常处理
 *
 * @author 小威老师 xiaoweijiagou@163.com
 *
 */

@RestControllerAdvice
public class ExceptionHandlerAdvice {

	private static final Logger log = LoggerFactory.getLogger("adminLogger");

	@ExceptionHandler({ IllegalArgumentException.class })
	@ResponseStatus(HttpStatus.BAD_REQUEST)
	public ResponseInfo badRequestException(IllegalArgumentException exception) {
		return ResponseUtil.error(HttpStatus.BAD_REQUEST.value(), exception.getMessage());
	}

	@ExceptionHandler({ UnknownAccountException.class, IncorrectCredentialsException.class })
	@ResponseStatus(HttpStatus.UNAUTHORIZED)
	public ResponseInfo loginException(Exception exception) {
		return ResponseUtil.error(HttpStatus.UNAUTHORIZED.value(), exception.getMessage());
	}

	@ExceptionHandler({ UnauthorizedException.class })
	@ResponseStatus(HttpStatus.FORBIDDEN)
	public ResponseInfo forbidden(Exception exception) {
		return ResponseUtil.error(HttpStatus.FORBIDDEN.value(), exception.getMessage());
	}

	@ExceptionHandler({ MissingServletRequestParameterException.class, HttpMessageNotReadableException.class,
			UnsatisfiedServletRequestParameterException.class, MethodArgumentTypeMismatchException.class })
	@ResponseStatus(HttpStatus.BAD_REQUEST)
	public ResponseInfo badRequestException(Exception exception) {
		return ResponseUtil.error(HttpStatus.BAD_REQUEST.value(), exception.getMessage());
	}

	@ExceptionHandler(Throwable.class)
	@ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
	public ResponseInfo throwable(Throwable throwable) {
		log.error("系统异常", throwable);
		return ResponseUtil.error(HttpStatus.INTERNAL_SERVER_ERROR.value(), throwable.getMessage());

	}

	@ExceptionHandler(MyException.class)
	@ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
	public ResponseInfo exception(MyException e) {
		log.error("系统异常", e);
		return ResponseUtil.error(e.getCode(), e.getMessage());

	}

}
