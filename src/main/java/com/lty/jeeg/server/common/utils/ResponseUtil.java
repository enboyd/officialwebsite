package com.lty.jeeg.server.common.utils;

import com.lty.jeeg.server.common.dto.ResponseInfo;
import com.lty.jeeg.server.common.enums.ResponseEnum;

/**
 * @author LTY
 */
public class ResponseUtil {

    public static ResponseInfo success(String message, Object object) {
        ResponseInfo responseInfo = new ResponseInfo();
        responseInfo.setCode(0);
        responseInfo.setMessage(message);
        responseInfo.setData(object);
        return responseInfo;
    }

    public static ResponseInfo success(Object object) {
        ResponseInfo responseInfo = new ResponseInfo();
        responseInfo.setCode(0);
        responseInfo.setMessage("成功");
        responseInfo.setData(object);
        return responseInfo;
    }

    public static ResponseInfo success() {
        return success(null);
    }

    public static ResponseInfo error(ResponseEnum responseEnum) {
        ResponseInfo responseInfo = new ResponseInfo();
        responseInfo.setCode(responseEnum.getCode());
        responseInfo.setMessage(responseEnum.getMsg());
        return responseInfo;
    }

    public static ResponseInfo error(Integer code, String msg) {
        ResponseInfo responseInfo = new ResponseInfo();
        responseInfo.setCode(code);
        responseInfo.setMessage(msg);
        return responseInfo;
    }

    public static ResponseInfo error(String msg) {
        return error(ResponseEnum.ERROR.getCode(), msg);
    }

    public static ResponseInfo error() {
        return error(ResponseEnum.ERROR.getMsg());
    }
}
