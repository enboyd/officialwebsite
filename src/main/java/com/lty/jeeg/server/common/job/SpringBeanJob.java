package com.lty.jeeg.server.common.job;

import com.lty.jeeg.server.common.config.JobConfig;
import com.lty.jeeg.server.modules.sys.service.JobService;
import lombok.extern.slf4j.Slf4j;
import org.quartz.JobExecutionContext;
import org.quartz.SchedulerException;
import org.springframework.context.ApplicationContext;
import org.springframework.scheduling.quartz.QuartzJobBean;

/**
 * @author LTY
 */
@Slf4j
public class SpringBeanJob extends QuartzJobBean {

	@Override
	protected void executeInternal(JobExecutionContext context) {
		try {
			ApplicationContext applicationContext = (ApplicationContext) context.getScheduler().getContext()
					.get(JobConfig.KEY);
			JobService jobService = applicationContext.getBean(JobService.class);
			jobService.doJob(context.getJobDetail().getJobDataMap());
		} catch (SchedulerException e) {
			e.printStackTrace();
		}
	}

}
