package com.lty.jeeg.server.common.dto;

import lombok.Data;

import java.io.Serializable;
import java.util.List;

/**
 * @author LTY
 */
@Data
public class GenerateInput implements Serializable {

	private static final long serialVersionUID = -2870071259702969061L;

	/**
	 * 保存路径
	 */
	private String path;

	/**
	 * 数据库表名
	 */
	private String tableName;

	/**
	 * 数据库表注释
	 */
	private String tableComment;

	/**
	 * 模块名
	 */
	private String moduleName;

	/**
	 * 类型：（0：普通，1：树结构）
	 */
	private Integer category;

	/**
	 * bean包名
	 */
	private String beanPackageName;

	/**
	 * java类名
	 */
	private String beanName;

	/**
	 * dao包名
	 */
	private String daoPackageName;

	/**
	 * dao类名
	 */
	private String daoName;

	/**
	 * service包名
	 */
	private String servicePkgName;

	/**
	 * service类名
	 */
	private String serviceName;

	/**
	 * service实现类包名
	 */
	private String serviceImplPkgName;

	/**
	 * service实现类名
	 */
	private String serviceImplName;

	/**
	 * controller包名
	 */
	private String controllerPkgName;

	/**
	 * controller类名
	 */
	private String controllerName;

	/**
	 * 数据库列名
	 */
	private List<String> columnNames;

	/**
	 * 字段名
	 */
	private List<String> beanFieldName;

	/**
	 * 字段类型
	 */
	private List<String> beanFieldType;

	/**
	 * 字段默认值
	 */
	private List<String> beanFieldValue;

	/**
	 * 字段备注
	 */
	private List<String> beanFieldComment;

}
