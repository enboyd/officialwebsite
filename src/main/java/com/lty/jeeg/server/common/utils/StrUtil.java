package com.lty.jeeg.server.common.utils;

import java.util.List;

import com.google.common.collect.Lists;
import org.apache.commons.lang3.StringUtils;

/**
 * 字符串转化工具类
 * 
 * @author 小威老师 xiaoweijiagou@163.com
 *
 */
public class StrUtil {

	/**
	 * 字符串转为驼峰
	 * 
	 * @param str
	 * @return
	 */
	public static String str2hump(String str, Boolean isLowerFirst) {
		StringBuilder builder = new StringBuilder();
		if (str != null && str.length() > 0) {
			if (str.contains("_")) {
				String[] chars = str.split("_");
				int size = chars.length;
				if (size > 0) {
					List<String> list = Lists.newArrayList();
					for (String s : chars) {
						if (s != null && s.trim().length() > 0) {
							list.add(s);
						}
					}

					size = list.size();
					if (size > 0) {
						for (int i = 0; i < size; i++) {
							String s = list.get(i);
							builder.append(s.substring(0, 1).toUpperCase());
							if (s.length() > 1) {
								builder.append(s.substring(1));
							}
						}
					}
				}
			} else {
				builder.append(str);
			}
		}

		if (isLowerFirst) {
			return lowerFirst(builder.toString());
		}
		return builder.toString();
	}

	public static String str2hump(String str) {
		return str2hump(str, false);
	}

	/**
	 * 将首字符转为小写
	 * @param str 要改写的字符串
	 * @return 改写后的字符串
	 */
	public static String lowerFirst(String str) {
		if (StringUtils.isNotBlank(str)) {
			String firstChar = str.substring(0, 1);
			return str.replaceFirst(firstChar, firstChar.toLowerCase());
		}
		return StringUtils.EMPTY;
	}

}
