package com.lty.jeeg.server.common.constant;

/**
 * @author LTY
 */
public class SysConstant {

    public static String COMMA_EN = ",";

    public static String COMMA_CN = "，";

    public static String SEMICOLON_EN = ";";

    public static String SEMICOLON_CN = "；";

    public static String PERIOD_EN = ".";

    public static String LEFT_SQUARE_BRACKETS = "[";

    public static String RIGHT_SQUARE_BRACKETS = "]";

    public static String UNDERLINE = "_";

    public static String SLASH = "/";

    public static String BACKSLASH = "\\";

    public static String SQL_ID = "id";

    public static String SQL_CREATE_BY = "createBy";

    public static String SQL_UPDATE_BY = "updateBy";

    public static String SQL_CREATE_TIME = "createTime";

    public static String SQL_UPDATE_TIME = "updateTime";

    public static String SQL_PARENTID = "parentId";

    public static String SQL_START = "start";

    public static String SQL_LENGTH = "length";

    public static String SQL_SELECT = "select";


}
