package com.lty.jeeg.server.common.vo;

import lombok.Data;

import java.io.Serializable;

/**
 * @author xuchen
 */
@Data
public class UserVO implements Serializable {

    private Long id;

    private String nickname;

    private String username;

    private String email;

}
