package com.lty.jeeg.server.common.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * @author LTY
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class ResponseInfo<T> implements Serializable {

	private static final long serialVersionUID = -4417715614021482064L;

	private Integer code;

	private String message;

	private T data;

}
