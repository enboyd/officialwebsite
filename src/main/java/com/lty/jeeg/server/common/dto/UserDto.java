package com.lty.jeeg.server.common.dto;

import com.lty.jeeg.server.modules.sys.model.Role;
import com.lty.jeeg.server.modules.sys.model.User;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.List;

/**
 * @author LTY
 */
@EqualsAndHashCode(callSuper = true)
@Data
public class UserDto extends User {

	private static final long serialVersionUID = -184009306207076712L;

	private Long deptId;

	private String deptName;

}
