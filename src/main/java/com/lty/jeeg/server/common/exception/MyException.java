package com.lty.jeeg.server.common.exception;

import com.lty.jeeg.server.common.dto.ResponseInfo;
import com.lty.jeeg.server.common.enums.ResponseEnum;
import lombok.Getter;

/**
 * @author LTY
 */
@Getter
public class MyException extends RuntimeException {

    private static final long serialVersionUID = -2656234462075093735L;

    private Integer code;

    public MyException(ResponseEnum responseEnum) {
        super(responseEnum.getMsg());
        this.code = responseEnum.getCode();
    }

    public MyException(Integer code, String msg) {
        super(msg);
        this.code = code;
    }

    public MyException(String msg) {
        super(msg);
        this.code = ResponseEnum.ERROR.getCode();
    }
}
