package com.lty.jeeg.server.common.utils;

import java.util.List;

import org.apache.shiro.SecurityUtils;
import org.apache.shiro.session.Session;
import org.apache.shiro.subject.Subject;

import com.lty.jeeg.server.common.constant.UserConstants;
import com.lty.jeeg.server.modules.sys.model.Permission;
import com.lty.jeeg.server.modules.sys.model.User;

/**
 * @author LTY
 */
public class UserUtil {

	public static User getCurrentUser() {
		return (User) getSession().getAttribute(UserConstants.LOGIN_USER);
	}

	public static void setUserSession(User user) {
		getSession().setAttribute(UserConstants.LOGIN_USER, user);
	}

	@SuppressWarnings("unchecked")
	public static List<Permission> getCurrentPermissions() {
		return (List<Permission>) getSession().getAttribute(UserConstants.USER_PERMISSIONS);
	}

	public static void setPermissionSession(List<Permission> list) {
		getSession().setAttribute(UserConstants.USER_PERMISSIONS, list);
	}

	public static Session getSession() {
		Subject currentUser = SecurityUtils.getSubject();
		return currentUser.getSession();
	}
}
