/*
 Navicat Premium Data Transfer

 Source Server         : localhost
 Source Server Type    : MySQL
 Source Server Version : 50716
 Source Host           : localhost:3306
 Source Schema         : jeeg_origin

 Target Server Type    : MySQL
 Target Server Version : 50716
 File Encoding         : 65001

 Date: 03/07/2019 11:35:10
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for file_info
-- ----------------------------
DROP TABLE IF EXISTS `file_info`;
CREATE TABLE `file_info`  (
  `id` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '文件md5',
  `contentType` varchar(128) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `size` int(11) NOT NULL,
  `path` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '物理路径',
  `url` varchar(1024) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `type` int(1) NOT NULL,
  `createTime` timestamp(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0) COMMENT '创建时间',
  `updateTime` datetime(0) NOT NULL,
  `fileName` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '文件名称',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for qrtz_blob_triggers
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_blob_triggers`;
CREATE TABLE `qrtz_blob_triggers`  (
  `SCHED_NAME` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL,
  `TRIGGER_NAME` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL,
  `TRIGGER_GROUP` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL,
  `BLOB_DATA` blob NULL,
  PRIMARY KEY (`SCHED_NAME`, `TRIGGER_NAME`, `TRIGGER_GROUP`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_bin ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for qrtz_calendars
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_calendars`;
CREATE TABLE `qrtz_calendars`  (
  `SCHED_NAME` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL,
  `CALENDAR_NAME` varchar(128) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL,
  `CALENDAR` blob NOT NULL,
  PRIMARY KEY (`SCHED_NAME`, `CALENDAR_NAME`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_bin ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for qrtz_cron_triggers
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_cron_triggers`;
CREATE TABLE `qrtz_cron_triggers`  (
  `SCHED_NAME` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL,
  `TRIGGER_NAME` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL,
  `TRIGGER_GROUP` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL,
  `CRON_EXPRESSION` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL,
  `TIME_ZONE_ID` varchar(80) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL,
  PRIMARY KEY (`SCHED_NAME`, `TRIGGER_NAME`, `TRIGGER_GROUP`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_bin ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for qrtz_fired_triggers
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_fired_triggers`;
CREATE TABLE `qrtz_fired_triggers`  (
  `SCHED_NAME` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL,
  `ENTRY_ID` varchar(128) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL,
  `TRIGGER_NAME` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL,
  `TRIGGER_GROUP` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL,
  `INSTANCE_NAME` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL,
  `FIRED_TIME` bigint(13) NOT NULL,
  `SCHED_TIME` bigint(13) NOT NULL,
  `PRIORITY` int(11) NOT NULL,
  `STATE` varchar(16) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL,
  `JOB_NAME` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL,
  `JOB_GROUP` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL,
  `IS_NONCONCURRENT` varchar(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL,
  `REQUESTS_RECOVERY` varchar(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL,
  PRIMARY KEY (`SCHED_NAME`, `ENTRY_ID`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_bin ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for qrtz_job_details
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_job_details`;
CREATE TABLE `qrtz_job_details`  (
  `SCHED_NAME` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL,
  `JOB_NAME` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL,
  `JOB_GROUP` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL,
  `DESCRIPTION` varchar(128) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL,
  `JOB_CLASS_NAME` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL,
  `IS_DURABLE` varchar(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL,
  `IS_NONCONCURRENT` varchar(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL,
  `IS_UPDATE_DATA` varchar(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL,
  `REQUESTS_RECOVERY` varchar(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL,
  `JOB_DATA` blob NULL,
  PRIMARY KEY (`SCHED_NAME`, `JOB_NAME`, `JOB_GROUP`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_bin ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for qrtz_locks
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_locks`;
CREATE TABLE `qrtz_locks`  (
  `SCHED_NAME` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL,
  `LOCK_NAME` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL,
  PRIMARY KEY (`SCHED_NAME`, `LOCK_NAME`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_bin ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of qrtz_locks
-- ----------------------------
INSERT INTO `qrtz_locks` VALUES ('adminQuartzScheduler', 'STATE_ACCESS');
INSERT INTO `qrtz_locks` VALUES ('adminQuartzScheduler', 'TRIGGER_ACCESS');
INSERT INTO `qrtz_locks` VALUES ('quartzScheduler', 'TRIGGER_ACCESS');

-- ----------------------------
-- Table structure for qrtz_paused_trigger_grps
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_paused_trigger_grps`;
CREATE TABLE `qrtz_paused_trigger_grps`  (
  `SCHED_NAME` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL,
  `TRIGGER_GROUP` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL,
  PRIMARY KEY (`SCHED_NAME`, `TRIGGER_GROUP`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_bin ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for qrtz_scheduler_state
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_scheduler_state`;
CREATE TABLE `qrtz_scheduler_state`  (
  `SCHED_NAME` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL,
  `INSTANCE_NAME` varchar(128) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL,
  `LAST_CHECKIN_TIME` bigint(13) NOT NULL,
  `CHECKIN_INTERVAL` bigint(13) NOT NULL,
  PRIMARY KEY (`SCHED_NAME`, `INSTANCE_NAME`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_bin ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for qrtz_simple_triggers
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_simple_triggers`;
CREATE TABLE `qrtz_simple_triggers`  (
  `SCHED_NAME` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL,
  `TRIGGER_NAME` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL,
  `TRIGGER_GROUP` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL,
  `REPEAT_COUNT` bigint(7) NOT NULL,
  `REPEAT_INTERVAL` bigint(12) NOT NULL,
  `TIMES_TRIGGERED` bigint(10) NOT NULL,
  PRIMARY KEY (`SCHED_NAME`, `TRIGGER_NAME`, `TRIGGER_GROUP`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_bin ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for qrtz_simprop_triggers
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_simprop_triggers`;
CREATE TABLE `qrtz_simprop_triggers`  (
  `SCHED_NAME` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL,
  `TRIGGER_NAME` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL,
  `TRIGGER_GROUP` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL,
  `STR_PROP_1` varchar(512) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL,
  `STR_PROP_2` varchar(512) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL,
  `STR_PROP_3` varchar(512) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL,
  `INT_PROP_1` int(11) NULL DEFAULT NULL,
  `INT_PROP_2` int(11) NULL DEFAULT NULL,
  `LONG_PROP_1` bigint(20) NULL DEFAULT NULL,
  `LONG_PROP_2` bigint(20) NULL DEFAULT NULL,
  `DEC_PROP_1` decimal(13, 4) NULL DEFAULT NULL,
  `DEC_PROP_2` decimal(13, 4) NULL DEFAULT NULL,
  `BOOL_PROP_1` varchar(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL,
  `BOOL_PROP_2` varchar(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL,
  PRIMARY KEY (`SCHED_NAME`, `TRIGGER_NAME`, `TRIGGER_GROUP`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_bin ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for qrtz_triggers
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_triggers`;
CREATE TABLE `qrtz_triggers`  (
  `SCHED_NAME` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL,
  `TRIGGER_NAME` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL,
  `TRIGGER_GROUP` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL,
  `JOB_NAME` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL,
  `JOB_GROUP` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL,
  `DESCRIPTION` varchar(128) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL,
  `NEXT_FIRE_TIME` bigint(13) NULL DEFAULT NULL,
  `PREV_FIRE_TIME` bigint(13) NULL DEFAULT NULL,
  `PRIORITY` int(11) NULL DEFAULT NULL,
  `TRIGGER_STATE` varchar(16) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL,
  `TRIGGER_TYPE` varchar(8) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL,
  `START_TIME` bigint(13) NOT NULL,
  `END_TIME` bigint(13) NULL DEFAULT NULL,
  `CALENDAR_NAME` varchar(128) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL,
  `MISFIRE_INSTR` smallint(2) NULL DEFAULT NULL,
  `JOB_DATA` blob NULL,
  PRIMARY KEY (`SCHED_NAME`, `TRIGGER_NAME`, `TRIGGER_GROUP`) USING BTREE,
  INDEX `SCHED_NAME`(`SCHED_NAME`, `JOB_NAME`, `JOB_GROUP`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_bin ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for sys_dept
-- ----------------------------
DROP TABLE IF EXISTS `sys_dept`;
CREATE TABLE `sys_dept`  (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `parentId` int(11) NOT NULL COMMENT '父级ID',
  `parentIds` varchar(2000) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL DEFAULT '0,' COMMENT '所有父级ID',
  `name` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL COMMENT '机构名称',
  `company` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '所属公司',
  `type` tinyint(1) NOT NULL DEFAULT 0 COMMENT '机构类型(0:总公司;1:分公司;2:部门;3:小组;4:其他)',
  `level` tinyint(1) NOT NULL DEFAULT 1 COMMENT '机构等级(1:一级;2:二级;3:三级;4:四级)',
  `primaryUserId` int(11) NULL DEFAULT NULL COMMENT '主负责人ID',
  `deputyUserId` int(11) NULL DEFAULT NULL COMMENT '副负责人ID',
  `remark` varchar(1000) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '备注',
  `createTime` timestamp(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0) COMMENT '创建时间',
  `sort` int(3) NOT NULL DEFAULT 100 COMMENT '排序',
  `editable` tinyint(1) NOT NULL DEFAULT 1 COMMENT '是否可修改',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 177 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_bin COMMENT = '机构部门' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_dept
-- ----------------------------
INSERT INTO `sys_dept` VALUES (1, 0, '0,', '总公司', 'beep', 0, 1, NULL, NULL, '', '2019-04-23 16:04:36', 10, 1);

-- ----------------------------
-- Table structure for sys_logs
-- ----------------------------
DROP TABLE IF EXISTS `sys_logs`;
CREATE TABLE `sys_logs`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `userId` int(11) NOT NULL,
  `module` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '模块名',
  `flag` tinyint(4) NOT NULL DEFAULT 1 COMMENT '成功失败',
  `remark` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL COMMENT '备注',
  `createTime` timestamp(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0) COMMENT '创建时间',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `userId`(`userId`) USING BTREE,
  INDEX `createTime`(`createTime`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 5994 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for sys_permission
-- ----------------------------
DROP TABLE IF EXISTS `sys_permission`;
CREATE TABLE `sys_permission`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `parentId` int(11) NOT NULL,
  `name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `css` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `href` varchar(1000) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `type` tinyint(1) NOT NULL,
  `permission` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `sort` int(11) NOT NULL,
  `editable` tinyint(1) NOT NULL DEFAULT 1,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 178 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_permission
-- ----------------------------
INSERT INTO `sys_permission` VALUES (2, 7, '用户', 'fa-user', 'pages/user/userList.html', 1, '', 200, 0);
INSERT INTO `sys_permission` VALUES (3, 2, '查询', '', '', 2, 'sys:user:query', 100, 0);
INSERT INTO `sys_permission` VALUES (4, 2, '新增', '', '', 2, 'sys:user:add', 100, 0);
INSERT INTO `sys_permission` VALUES (7, 0, '系统设置', 'fa-gears', '', 1, '', 90, 0);
INSERT INTO `sys_permission` VALUES (8, 7, '菜单权限', 'fa-cog', 'pages/menu/menuList.html', 1, '', 6, 0);
INSERT INTO `sys_permission` VALUES (9, 8, '查询', '', '', 2, 'sys:menu:query', 100, 0);
INSERT INTO `sys_permission` VALUES (10, 8, '新增', '', '', 2, 'sys:menu:add', 100, 0);
INSERT INTO `sys_permission` VALUES (11, 8, '删除', '', '', 2, 'sys:menu:del', 100, 0);
INSERT INTO `sys_permission` VALUES (12, 7, '角色', 'fa-user-secret', 'pages/role/roleList.html', 1, '', 7, 0);
INSERT INTO `sys_permission` VALUES (13, 12, '查询', '', '', 2, 'sys:role:query', 100, 0);
INSERT INTO `sys_permission` VALUES (14, 12, '新增', '', '', 2, 'sys:role:add', 100, 0);
INSERT INTO `sys_permission` VALUES (15, 12, '删除', '', '', 2, 'sys:role:del', 100, 0);
INSERT INTO `sys_permission` VALUES (16, 0, '文件管理', 'fa-folder-open', 'pages/file/fileList.html', 1, '', 50, 0);
INSERT INTO `sys_permission` VALUES (17, 16, '查询', '', '', 2, 'sys:file:query', 100, 0);
INSERT INTO `sys_permission` VALUES (18, 16, '删除', '', '', 2, 'sys:file:del', 100, 0);
INSERT INTO `sys_permission` VALUES (19, 49, '数据源监控', 'fa-eye', 'druid/index.html', 1, '', 9, 0);
INSERT INTO `sys_permission` VALUES (20, 49, '接口swagger', 'fa-stumbleupon', 'swagger-ui.html', 1, '', 10, 0);
INSERT INTO `sys_permission` VALUES (21, 49, '代码生成', 'fa-wrench', 'pages/generate/edit.html', 1, 'generate:edit', 11, 0);
INSERT INTO `sys_permission` VALUES (22, 0, '公告管理', 'fa-info', 'pages/notice/noticeList.html', 1, '', 60, 0);
INSERT INTO `sys_permission` VALUES (23, 22, '查询', '', '', 2, 'notice:query', 100, 0);
INSERT INTO `sys_permission` VALUES (24, 22, '添加', '', '', 2, 'notice:add', 100, 0);
INSERT INTO `sys_permission` VALUES (25, 22, '删除', '', '', 2, 'notice:del', 100, 0);
INSERT INTO `sys_permission` VALUES (26, 49, '日志查询', 'fa-file-o', 'pages/log/logList.html', 1, 'sys:log:query', 13, 0);
INSERT INTO `sys_permission` VALUES (27, 0, '邮件管理', 'fa-envelope', 'pages/mail/mailList.html', 1, '', 70, 0);
INSERT INTO `sys_permission` VALUES (28, 27, '发送邮件', '', '', 2, 'mail:send', 100, 0);
INSERT INTO `sys_permission` VALUES (29, 27, '查询', '', '', 2, 'mail:all:query', 100, 0);
INSERT INTO `sys_permission` VALUES (30, 49, '定时任务管理', 'fa-clock-o', 'pages/job/jobList.html', 1, '', 15, 0);
INSERT INTO `sys_permission` VALUES (31, 30, '查询', '', '', 2, 'job:query', 100, 0);
INSERT INTO `sys_permission` VALUES (32, 30, '新增', '', '', 2, 'job:add', 100, 0);
INSERT INTO `sys_permission` VALUES (33, 30, '删除', '', '', 2, 'job:del', 100, 0);
INSERT INTO `sys_permission` VALUES (34, 49, 'excel导出', 'fa-arrow-circle-down', 'pages/excel/sql.html', 1, '', 16, 0);
INSERT INTO `sys_permission` VALUES (35, 34, '导出', '', '', 2, 'excel:down', 100, 0);
INSERT INTO `sys_permission` VALUES (36, 34, '页面显示数据', '', '', 2, 'excel:show:datas', 100, 0);
INSERT INTO `sys_permission` VALUES (37, 0, '字典管理', 'fa-book', 'pages/dict/dictList.html', 1, '', 80, 0);
INSERT INTO `sys_permission` VALUES (38, 37, '查询', '', '', 2, 'dict:query', 100, 0);
INSERT INTO `sys_permission` VALUES (39, 37, '新增', '', '', 2, 'dict:add', 100, 0);
INSERT INTO `sys_permission` VALUES (40, 37, '删除', '', '', 2, 'dict:del', 100, 0);
INSERT INTO `sys_permission` VALUES (42, 2, '删除', 'fa-trash-o', '', 2, 'sys:user:del', 100, 0);
INSERT INTO `sys_permission` VALUES (43, 7, '部门', 'fa-sitemap', 'pages/dept/deptList.html', 1, '', 100, 0);
INSERT INTO `sys_permission` VALUES (44, 43, '查询', '', '', 2, 'sys:dept:query', 100, 0);
INSERT INTO `sys_permission` VALUES (45, 43, '新增', '', '', 2, 'sys:dept:add', 200, 0);
INSERT INTO `sys_permission` VALUES (47, 43, '删除', '', '', 2, 'sys:dept:del', 400, 0);
INSERT INTO `sys_permission` VALUES (48, 43, '修改', '', '', 2, 'sys:dept:edit', 300, 0);
INSERT INTO `sys_permission` VALUES (49, 0, '开发运维', 'fa-git', '', 1, '', 100, 0);
INSERT INTO `sys_permission` VALUES (51, 130, '商品类型', '', 'pages/fas/goodsCategory/goodsCategoryList.html', 1, '', 10, 1);
INSERT INTO `sys_permission` VALUES (52, 51, '查询', '', '', 2, 'fas:goodsCategory:query', 100, 1);
INSERT INTO `sys_permission` VALUES (53, 51, '新增', '', '', 2, 'fas:goodsCategory:add', 200, 1);
INSERT INTO `sys_permission` VALUES (54, 51, '修改', '', '', 2, 'fas:goodsCategory:edit', 300, 1);
INSERT INTO `sys_permission` VALUES (55, 51, '删除', '', '', 2, 'fas:goodsCategory:del', 400, 1);
INSERT INTO `sys_permission` VALUES (56, 130, '商品', '', '/pages/fas/goods/goodsList.html', 1, '', 20, 1);
INSERT INTO `sys_permission` VALUES (57, 56, '查询', '', '', 2, 'fas:goods:query', 100, 1);
INSERT INTO `sys_permission` VALUES (58, 56, '新增', '', '', 2, 'fas:goods:add', 100, 1);
INSERT INTO `sys_permission` VALUES (59, 56, '修改', '', '', 2, 'fas:goods:edit', 100, 1);
INSERT INTO `sys_permission` VALUES (60, 56, '删除', '', '', 2, 'fas:goods:del', 100, 1);
INSERT INTO `sys_permission` VALUES (61, 130, '供应商', '', 'pages/fas/supplier/supplierList.html', 1, '', 30, 1);
INSERT INTO `sys_permission` VALUES (62, 61, '查询', '', '', 2, 'fas:supplier:query', 30, 1);
INSERT INTO `sys_permission` VALUES (63, 61, '新增', '', '', 2, 'fas:supplier:add', 60, 1);
INSERT INTO `sys_permission` VALUES (64, 61, '修改', '', '', 2, 'fas:supplier:edit', 100, 1);
INSERT INTO `sys_permission` VALUES (65, 61, '删除', '', '', 2, 'fas:supplier:del', 120, 1);
INSERT INTO `sys_permission` VALUES (67, 78, '查询', '', '', 2, 'fas:stock:query', 10, 1);
INSERT INTO `sys_permission` VALUES (68, 78, '新增', '', '', 2, 'fas:stock:add', 20, 1);
INSERT INTO `sys_permission` VALUES (69, 78, '修改', '', '', 2, 'fas:stock:edit', 30, 1);
INSERT INTO `sys_permission` VALUES (70, 78, '删除', '', '', 2, 'fas:stock:del', 40, 1);
INSERT INTO `sys_permission` VALUES (71, 131, '调拨', '', 'pages/fas/handOver/addHandOver.html', 1, '', 50, 1);
INSERT INTO `sys_permission` VALUES (74, 131, '采购与领用', '', '', 1, '', 60, 1);
INSERT INTO `sys_permission` VALUES (75, 74, '需求申请列表', '', 'pages/fas/order/orderList.html', 1, 'fas:order:query', 20, 1);
INSERT INTO `sys_permission` VALUES (76, 74, '待交付列表', '', 'pages/fas/orderDetail/orderDetailList.html', 1, '', 30, 1);
INSERT INTO `sys_permission` VALUES (77, 74, '添加需求单', '', 'pages/fas/order/addOrder.html', 1, '', 10, 1);
INSERT INTO `sys_permission` VALUES (78, 66, '库存合计列表', '', 'pages/fas/stock/stockList.html', 1, '', 30, 1);
INSERT INTO `sys_permission` VALUES (79, 66, '部门库存明细列表', '', 'pages/fas/stock/stockDList.html', 1, '', 20, 1);
INSERT INTO `sys_permission` VALUES (80, 66, '个人库存明细列表', '', 'pages/fas/stock/stockPList.html', 1, '', 10, 1);
INSERT INTO `sys_permission` VALUES (81, 79, '查询', '', '', 2, 'fas:stockD:query', 100, 1);
INSERT INTO `sys_permission` VALUES (82, 79, '新增', '', '', 2, 'fas:stockD:add', 100, 1);
INSERT INTO `sys_permission` VALUES (83, 79, '修改', '', '', 2, 'fas:stockD:edit', 100, 1);
INSERT INTO `sys_permission` VALUES (84, 79, '删除', '', '', 2, 'fas:stockD:del', 100, 1);
INSERT INTO `sys_permission` VALUES (85, 80, '查询', '', '', 2, 'fas:stockP:query', 100, 1);
INSERT INTO `sys_permission` VALUES (86, 80, '新增', '', '', 2, 'fas:stockP:add', 100, 1);
INSERT INTO `sys_permission` VALUES (87, 80, '修改', '', '', 2, 'fas:stockP:edit', 100, 1);
INSERT INTO `sys_permission` VALUES (88, 80, '删除', '', '', 2, 'fas:stockP:del', 100, 1);
INSERT INTO `sys_permission` VALUES (89, 56, '下载', '', '', 2, 'fas:goods:export', 100, 1);
INSERT INTO `sys_permission` VALUES (90, 75, '提交', '', '', 2, 'fas:order:submit', 100, 1);
INSERT INTO `sys_permission` VALUES (91, 75, '撤回', '', '', 2, 'fas:order:withdraw', 100, 1);
INSERT INTO `sys_permission` VALUES (92, 75, '确认', '', '', 2, 'fas:order:confirm', 100, 1);
INSERT INTO `sys_permission` VALUES (93, 75, '驳回', '', '', 2, 'fas:order:reject', 100, 1);
INSERT INTO `sys_permission` VALUES (95, 76, '取消交付', '', '', 2, 'fas:order:cancel', 100, 1);
INSERT INTO `sys_permission` VALUES (96, 76, '交付', '', '', 2, 'fas:order:deliver', 100, 1);
INSERT INTO `sys_permission` VALUES (97, 56, '导入', '', '', 2, 'fas:goods:importData', 100, 1);
INSERT INTO `sys_permission` VALUES (98, 16, '下载', '', '', 2, 'sys:file:down', 100, 1);
INSERT INTO `sys_permission` VALUES (99, 80, '导出', '', '', 2, 'fas:stockP:export', 100, 1);
INSERT INTO `sys_permission` VALUES (100, 79, '导出', '', '', 2, 'fas:stockD:export', 100, 1);
INSERT INTO `sys_permission` VALUES (101, 78, '导出', '', '', 2, 'fas:stock:export', 100, 1);
INSERT INTO `sys_permission` VALUES (103, 75, '导出', '', '', 2, 'fas:order:export', 100, 1);
INSERT INTO `sys_permission` VALUES (106, 131, '借用', '', 'pages/fas/borrow/borrowList.html', 1, '', 90, 1);
INSERT INTO `sys_permission` VALUES (107, 131, '置换与报废', '', 'pages/fas/handOver/discardList.html', 1, '', 70, 1);
INSERT INTO `sys_permission` VALUES (114, 106, '添加', '', '', 2, 'fas:borrow:add', 100, 1);
INSERT INTO `sys_permission` VALUES (116, 106, '修改', '', '', 2, 'fas:borrow:edit', 100, 1);
INSERT INTO `sys_permission` VALUES (117, 106, '删除', '', '', 2, 'fas:borrow:del', 100, 1);
INSERT INTO `sys_permission` VALUES (118, 106, '导出', '', '', 2, 'fas:borrow:export', 100, 1);
INSERT INTO `sys_permission` VALUES (119, 106, '提交', '', '', 2, 'fas:borrow:submission', 100, 1);
INSERT INTO `sys_permission` VALUES (120, 106, '撤回', '', '', 2, 'fas:borrow:withdraw', 100, 1);
INSERT INTO `sys_permission` VALUES (121, 106, '确认', '', '', 2, 'fas:borrow:adopt', 100, 1);
INSERT INTO `sys_permission` VALUES (122, 106, '归还', '', '', 2, 'fas:borrow:revert', 100, 1);
INSERT INTO `sys_permission` VALUES (124, 123, '导出', '', '', 2, 'fas:handOver:export', 10, 1);
INSERT INTO `sys_permission` VALUES (125, 107, '提交', '', '', 2, 'fas:discard:submit', 10, 1);
INSERT INTO `sys_permission` VALUES (126, 107, '撤回', '', '', 2, 'fas:discard:withdraw', 20, 1);
INSERT INTO `sys_permission` VALUES (127, 107, '驳回', '', '', 2, 'fas:discard:reject', 30, 1);
INSERT INTO `sys_permission` VALUES (128, 107, '审批通过 ', '', '', 2, 'fas:discard:confirm', 40, 1);
INSERT INTO `sys_permission` VALUES (129, 80, '导入', '', '', 2, 'fas:stock:importData', 100, 1);
INSERT INTO `sys_permission` VALUES (136, 77, '新增', '', '', 2, 'fas:order:add', 30, 1);
INSERT INTO `sys_permission` VALUES (137, 107, '修改', '', '', 2, 'fas:discard:edit', 50, 1);
INSERT INTO `sys_permission` VALUES (138, 107, '删除', '', '', 2, 'fas:discard:del', 70, 1);
INSERT INTO `sys_permission` VALUES (140, 107, '添加', '', '', 2, 'fas:discard:add', 80, 1);
INSERT INTO `sys_permission` VALUES (141, 71, '查询', '', '', 2, 'fas:handOver:query', 10, 1);
INSERT INTO `sys_permission` VALUES (142, 75, '修改', '', '', 2, 'fas:order:edit', 100, 1);
INSERT INTO `sys_permission` VALUES (143, 75, '查看', '', '', 2, 'fas:order:query', 100, 1);
INSERT INTO `sys_permission` VALUES (144, 107, '查看', '', '', 2, 'fas:discard:list', 100, 1);
INSERT INTO `sys_permission` VALUES (145, 79, '修改闲置数量', '', '', 2, 'fas:stockD:xgxz', 100, 1);
INSERT INTO `sys_permission` VALUES (146, 66, '报废仓', '', 'pages/fas/stock/stockDiscardList.html', 1, '', 50, 1);
INSERT INTO `sys_permission` VALUES (147, 66, '行政仓', '', 'pages/fas/stock/stockAdminList.html', 1, '', 40, 1);
INSERT INTO `sys_permission` VALUES (148, 75, '删除', '', '', 2, 'fas:order:del', 100, 1);
INSERT INTO `sys_permission` VALUES (149, 131, '盘盈盘亏', '', 'pages/fas/surplusLosses/surplusLossesList.html', 1, '', 200, 1);
INSERT INTO `sys_permission` VALUES (150, 149, '添加', '', '', 2, 'fas:surplusLosses:add', 100, 1);
INSERT INTO `sys_permission` VALUES (151, 149, '修改', '', '', 2, 'fas:surplusLosses:edit', 110, 1);
INSERT INTO `sys_permission` VALUES (152, 149, '删除', '', '', 2, 'fas:surplusLosses:del', 120, 1);
INSERT INTO `sys_permission` VALUES (153, 106, '列表', '', '', 2, 'fas:borrow:list', 100, 1);
INSERT INTO `sys_permission` VALUES (154, 149, '列表', '', '', 2, 'fas:surplusLosses:add', 140, 1);
INSERT INTO `sys_permission` VALUES (161, 156, '查询', '', '', 2, 'tms:project:query', 10, 1);
INSERT INTO `sys_permission` VALUES (162, 156, '新增', '', '', 2, 'tms:project:add', 40, 1);
INSERT INTO `sys_permission` VALUES (163, 156, '修改', '', '', 2, 'tms:project:edit', 70, 1);
INSERT INTO `sys_permission` VALUES (164, 156, '删除', '', '', 2, 'tms:project:del', 100, 1);
INSERT INTO `sys_permission` VALUES (166, 165, '部门', '', 'pages/fas/dept/deptFasList.html', 1, '', 100, 1);
INSERT INTO `sys_permission` VALUES (167, 166, '修改', '', '', 2, 'sys:deptFas:edit', 100, 1);
INSERT INTO `sys_permission` VALUES (168, 166, '新增', '', '', 2, 'sys:deptFas:add', 100, 1);
INSERT INTO `sys_permission` VALUES (169, 166, '删除', '', '', 2, 'sys:deptFas:del', 100, 1);
INSERT INTO `sys_permission` VALUES (171, 0, '个人中心', '', '', 2, '', 20, 1);
INSERT INTO `sys_permission` VALUES (177, 171, '个人信息修改', '', '', 2, 'sys:user:add', 100, 1);

-- ----------------------------
-- Table structure for sys_role
-- ----------------------------
DROP TABLE IF EXISTS `sys_role`;
CREATE TABLE `sys_role`  (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL COMMENT '名称',
  `description` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '描述',
  `dataScope` tinyint(1) NOT NULL DEFAULT 8 COMMENT '数据范围',
  `usable` tinyint(1) NOT NULL DEFAULT 1 COMMENT '是否可用',
  `remark` varchar(1000) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '备注',
  `createTime` timestamp(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0) COMMENT '创建时间',
  `updateTime` timestamp(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0) ON UPDATE CURRENT_TIMESTAMP(0) COMMENT '更新时间',
  `createBy` int(11) NOT NULL COMMENT '创建人',
  `updateBy` int(11) NOT NULL COMMENT '修改人',
  `isDeleted` tinyint(1) NOT NULL DEFAULT 0 COMMENT '是否删除',
  `editable` tinyint(1) NOT NULL DEFAULT 1 COMMENT '是否可修改',
  `modular` varchar(12) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '模块',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `name`(`name`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 16 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_role
-- ----------------------------
INSERT INTO `sys_role` VALUES (1, '系统管理员', '管理员', 1, 1, NULL, '2017-05-01 13:25:39', '2019-07-03 09:11:03', 1, 1, 0, 1, 'sys');
INSERT INTO `sys_role` VALUES (15, '普通用户', '', 8, 1, NULL, '2019-07-03 09:41:50', '2019-07-03 09:41:50', 1, 1, 0, 1, NULL);

-- ----------------------------
-- Table structure for sys_role_permission
-- ----------------------------
DROP TABLE IF EXISTS `sys_role_permission`;
CREATE TABLE `sys_role_permission`  (
  `roleId` int(11) NOT NULL,
  `permissionId` int(11) NOT NULL,
  PRIMARY KEY (`roleId`, `permissionId`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_role_permission
-- ----------------------------
INSERT INTO `sys_role_permission` VALUES (1, 2);
INSERT INTO `sys_role_permission` VALUES (1, 3);
INSERT INTO `sys_role_permission` VALUES (1, 4);
INSERT INTO `sys_role_permission` VALUES (1, 7);
INSERT INTO `sys_role_permission` VALUES (1, 8);
INSERT INTO `sys_role_permission` VALUES (1, 9);
INSERT INTO `sys_role_permission` VALUES (1, 10);
INSERT INTO `sys_role_permission` VALUES (1, 11);
INSERT INTO `sys_role_permission` VALUES (1, 12);
INSERT INTO `sys_role_permission` VALUES (1, 13);
INSERT INTO `sys_role_permission` VALUES (1, 14);
INSERT INTO `sys_role_permission` VALUES (1, 15);
INSERT INTO `sys_role_permission` VALUES (1, 16);
INSERT INTO `sys_role_permission` VALUES (1, 17);
INSERT INTO `sys_role_permission` VALUES (1, 18);
INSERT INTO `sys_role_permission` VALUES (1, 19);
INSERT INTO `sys_role_permission` VALUES (1, 20);
INSERT INTO `sys_role_permission` VALUES (1, 21);
INSERT INTO `sys_role_permission` VALUES (1, 22);
INSERT INTO `sys_role_permission` VALUES (1, 23);
INSERT INTO `sys_role_permission` VALUES (1, 24);
INSERT INTO `sys_role_permission` VALUES (1, 25);
INSERT INTO `sys_role_permission` VALUES (1, 26);
INSERT INTO `sys_role_permission` VALUES (1, 27);
INSERT INTO `sys_role_permission` VALUES (1, 28);
INSERT INTO `sys_role_permission` VALUES (1, 29);
INSERT INTO `sys_role_permission` VALUES (1, 30);
INSERT INTO `sys_role_permission` VALUES (1, 31);
INSERT INTO `sys_role_permission` VALUES (1, 32);
INSERT INTO `sys_role_permission` VALUES (1, 33);
INSERT INTO `sys_role_permission` VALUES (1, 34);
INSERT INTO `sys_role_permission` VALUES (1, 35);
INSERT INTO `sys_role_permission` VALUES (1, 36);
INSERT INTO `sys_role_permission` VALUES (1, 37);
INSERT INTO `sys_role_permission` VALUES (1, 38);
INSERT INTO `sys_role_permission` VALUES (1, 39);
INSERT INTO `sys_role_permission` VALUES (1, 40);
INSERT INTO `sys_role_permission` VALUES (1, 42);
INSERT INTO `sys_role_permission` VALUES (1, 43);
INSERT INTO `sys_role_permission` VALUES (1, 44);
INSERT INTO `sys_role_permission` VALUES (1, 45);
INSERT INTO `sys_role_permission` VALUES (1, 47);
INSERT INTO `sys_role_permission` VALUES (1, 48);
INSERT INTO `sys_role_permission` VALUES (1, 49);
INSERT INTO `sys_role_permission` VALUES (1, 98);
INSERT INTO `sys_role_permission` VALUES (1, 171);
INSERT INTO `sys_role_permission` VALUES (1, 177);
INSERT INTO `sys_role_permission` VALUES (15, 16);
INSERT INTO `sys_role_permission` VALUES (15, 17);
INSERT INTO `sys_role_permission` VALUES (15, 22);
INSERT INTO `sys_role_permission` VALUES (15, 23);
INSERT INTO `sys_role_permission` VALUES (15, 27);
INSERT INTO `sys_role_permission` VALUES (15, 29);
INSERT INTO `sys_role_permission` VALUES (15, 171);
INSERT INTO `sys_role_permission` VALUES (15, 177);

-- ----------------------------
-- Table structure for sys_role_user
-- ----------------------------
DROP TABLE IF EXISTS `sys_role_user`;
CREATE TABLE `sys_role_user`  (
  `userId` int(11) NOT NULL COMMENT '用户ID',
  `roleId` int(11) NOT NULL COMMENT '角色ID',
  PRIMARY KEY (`userId`, `roleId`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_role_user
-- ----------------------------
INSERT INTO `sys_role_user` VALUES (1, 1);
INSERT INTO `sys_role_user` VALUES (88, 1);

-- ----------------------------
-- Table structure for sys_user
-- ----------------------------
DROP TABLE IF EXISTS `sys_user`;
CREATE TABLE `sys_user`  (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `username` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '登录名',
  `password` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '密码',
  `salt` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '盐',
  `nickname` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '无名氏' COMMENT '昵称',
  `role` int(11) NULL DEFAULT NULL COMMENT '角色',
  `deptId` int(11) NOT NULL DEFAULT 0 COMMENT '部门',
  `headImgUrl` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '头像',
  `phone` varchar(11) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '电话',
  `telephone` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '手机',
  `email` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '电子邮箱',
  `birthday` date NULL DEFAULT NULL COMMENT '生日',
  `sex` tinyint(1) NOT NULL DEFAULT 0 COMMENT '性别',
  `remark` varchar(1000) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '备注',
  `status` tinyint(1) NOT NULL DEFAULT 1 COMMENT '状态',
  `createTime` timestamp(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0) COMMENT '创建时间',
  `updateTime` timestamp(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0) ON UPDATE CURRENT_TIMESTAMP(0) COMMENT '更新时间',
  `createBy` int(11) NOT NULL COMMENT '创建人',
  `updateBy` int(11) NOT NULL COMMENT '修改人',
  `isDeleted` tinyint(1) NOT NULL DEFAULT 0 COMMENT '删除标记',
  `editable` tinyint(1) NOT NULL DEFAULT 1 COMMENT '是否可修改',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `username`(`username`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1364 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_user
-- ----------------------------
INSERT INTO `sys_user` VALUES (1, 'admin', '4b4b6fef5a941a967181fc686f7ab6b4', '7fe3487e8776aa074e3fdf0b54fd7da3', '系统管理员', 1, 1, '/1/2019/03/07/fe9ad9d6afee33a7300d3a4ce0b25b95.gif', NULL, NULL, NULL, NULL, 0, '最高管理员', 1, '2019-03-18 09:37:32', '2019-04-23 16:02:29', 1, 1, 0, 0);

-- ----------------------------
-- Table structure for t_dict
-- ----------------------------
DROP TABLE IF EXISTS `t_dict`;
CREATE TABLE `t_dict`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type` varchar(16) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `k` varchar(16) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `val` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `editable` tinyint(1) NULL DEFAULT 1 COMMENT '是否可编辑',
  `createTime` timestamp(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0) COMMENT '创建时间',
  `updateTime` timestamp(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0) ON UPDATE CURRENT_TIMESTAMP(0) COMMENT '更新时间',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `type`(`type`, `k`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 79 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of t_dict
-- ----------------------------
INSERT INTO `t_dict` VALUES (1, 'sex', '0', '女', 0, '2017-11-17 09:58:24', '2017-11-18 14:21:05');
INSERT INTO `t_dict` VALUES (2, 'sex', '1', '男', 0, '2017-11-17 10:03:46', '2017-11-17 10:03:46');
INSERT INTO `t_dict` VALUES (3, 'userStatus', '0', '离职', 0, '2017-11-17 16:26:06', '2018-11-29 16:16:09');
INSERT INTO `t_dict` VALUES (4, 'userStatus', '1', '正常', 0, '2017-11-17 16:26:06', '2018-11-28 17:31:55');
INSERT INTO `t_dict` VALUES (5, 'userStatus', '2', '锁定', 0, '2017-11-17 16:26:06', '2017-11-17 16:26:09');
INSERT INTO `t_dict` VALUES (6, 'noticeStatus', '0', '草稿', 0, '2017-11-17 16:26:06', '2017-11-17 16:26:09');
INSERT INTO `t_dict` VALUES (7, 'noticeStatus', '1', '发布', 0, '2017-11-17 16:26:06', '2017-11-17 16:26:09');
INSERT INTO `t_dict` VALUES (8, 'isRead', '0', '未读', 0, '2017-11-17 16:26:06', '2017-11-17 16:26:09');
INSERT INTO `t_dict` VALUES (9, 'isRead', '1', '已读', 0, '2017-11-17 16:26:06', '2017-11-17 16:26:09');
INSERT INTO `t_dict` VALUES (10, 'dept_type', '0', '总公司', 0, '2018-12-05 14:55:09', '2018-12-05 14:55:09');
INSERT INTO `t_dict` VALUES (11, 'dept_type', '1', '分公司', 0, '2018-12-05 14:55:22', '2018-12-05 14:55:22');
INSERT INTO `t_dict` VALUES (12, 'dept_type', '2', '部门', 0, '2018-12-05 14:55:30', '2018-12-05 14:55:30');
INSERT INTO `t_dict` VALUES (13, 'dept_type', '3', '小组', 0, '2018-12-05 14:55:54', '2018-12-05 14:55:54');
INSERT INTO `t_dict` VALUES (14, 'dept_type', '4', '其他', 0, '2018-12-05 14:56:07', '2018-12-05 14:56:07');
INSERT INTO `t_dict` VALUES (15, 'dept_level', '1', '一级', 1, '2018-12-05 14:57:08', '2018-12-05 14:57:08');
INSERT INTO `t_dict` VALUES (16, 'dept_level', '2', '二级', 1, '2018-12-05 14:57:15', '2018-12-05 14:57:15');
INSERT INTO `t_dict` VALUES (17, 'dept_level', '3', '三级', 1, '2018-12-05 14:57:21', '2018-12-05 14:57:21');
INSERT INTO `t_dict` VALUES (18, 'dept_level', '4', '四级', 1, '2018-12-05 14:57:29', '2018-12-05 14:57:29');
INSERT INTO `t_dict` VALUES (19, 'boolean', '1', '是', 0, '2019-01-02 08:17:39', '2019-01-02 08:17:39');
INSERT INTO `t_dict` VALUES (20, 'boolean', '0', '否', 0, '2019-01-02 08:17:56', '2019-01-02 08:17:56');

-- ----------------------------
-- Table structure for t_job
-- ----------------------------
DROP TABLE IF EXISTS `t_job`;
CREATE TABLE `t_job`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `jobName` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `description` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `cron` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `springBeanName` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT 'springBean名',
  `methodName` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '方法名',
  `isSysJob` tinyint(1) NOT NULL COMMENT '是否是系统job',
  `status` tinyint(1) NOT NULL DEFAULT 1,
  `createTime` timestamp(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0) COMMENT '创建时间',
  `updateTime` timestamp(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0) ON UPDATE CURRENT_TIMESTAMP(0) COMMENT '更新时间',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `jobName`(`jobName`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 8 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for t_mail
-- ----------------------------
DROP TABLE IF EXISTS `t_mail`;
CREATE TABLE `t_mail`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `userId` int(11) NOT NULL COMMENT '发送人',
  `subject` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '标题',
  `content` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '正文',
  `createTime` timestamp(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0) COMMENT '创建时间',
  `updateTime` timestamp(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0) ON UPDATE CURRENT_TIMESTAMP(0) COMMENT '更新时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 12 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of t_mail
-- ----------------------------
INSERT INTO `t_mail` VALUES (9, 1, 'jeeg mail test', '邮件测试', '2018-11-27 16:41:34', '2018-11-27 16:41:34');
INSERT INTO `t_mail` VALUES (10, 1, 'jeeg mail test', '欢迎注册', '2018-11-27 16:47:53', '2018-11-27 16:47:53');
INSERT INTO `t_mail` VALUES (11, 1, '欢迎加入jeeg', '欢迎加入jeeg大家庭!详情咨询enboyd@163.com', '2018-11-28 17:23:35', '2018-11-28 17:23:35');

-- ----------------------------
-- Table structure for t_mail_to
-- ----------------------------
DROP TABLE IF EXISTS `t_mail_to`;
CREATE TABLE `t_mail_to`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `mailId` int(11) NOT NULL,
  `toUser` varchar(128) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT 1 COMMENT '1成功，0失败',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 19 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of t_mail_to
-- ----------------------------
INSERT INTO `t_mail_to` VALUES (16, 9, 'enboyd@163.com', 0);
INSERT INTO `t_mail_to` VALUES (17, 10, 'enboyd@163.com', 1);
INSERT INTO `t_mail_to` VALUES (18, 11, 'enboyd@163.com', 1);

-- ----------------------------
-- Table structure for t_notice
-- ----------------------------
DROP TABLE IF EXISTS `t_notice`;
CREATE TABLE `t_notice`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(128) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `content` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT 1,
  `createBy` int(11) NOT NULL DEFAULT 1 COMMENT '发送者',
  `createTime` timestamp(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0) COMMENT '创建时间',
  `updateTime` timestamp(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0) ON UPDATE CURRENT_TIMESTAMP(0) COMMENT '更新时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 11 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for t_notice_read
-- ----------------------------
DROP TABLE IF EXISTS `t_notice_read`;
CREATE TABLE `t_notice_read`  (
  `noticeId` int(11) NOT NULL COMMENT '通知ID',
  `userId` int(11) NOT NULL COMMENT '用户ID',
  `isRead` tinyint(3) NOT NULL DEFAULT 0 COMMENT '是否已读',
  `readTime` timestamp(0) NULL DEFAULT NULL COMMENT '读取时间',
  PRIMARY KEY (`noticeId`, `userId`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for t_status_flow
-- ----------------------------
DROP TABLE IF EXISTS `t_status_flow`;
CREATE TABLE `t_status_flow`  (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `type` varchar(16) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL COMMENT '类型',
  `k` varchar(16) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT 'key',
  `val` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL COMMENT 'value',
  `prev` int(11) NULL DEFAULT NULL COMMENT '上一项',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `t_status_flow_uk`(`type`, `k`, `prev`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 12 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_bin COMMENT = '状态流' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of t_status_flow
-- ----------------------------
INSERT INTO `t_status_flow` VALUES (1, 'task_status', '0', '未开始', NULL);
INSERT INTO `t_status_flow` VALUES (2, 'task_status', '2', '暂停', NULL);
INSERT INTO `t_status_flow` VALUES (3, 'task_status', '4', '进行中', 0);
INSERT INTO `t_status_flow` VALUES (4, 'task_status', '-1', '取消', 0);
INSERT INTO `t_status_flow` VALUES (5, 'task_status', '7', '已完成', 4);
INSERT INTO `t_status_flow` VALUES (6, 'task_status', '2', '暂停', 4);
INSERT INTO `t_status_flow` VALUES (7, 'task_status', '-1', '取消', 4);
INSERT INTO `t_status_flow` VALUES (8, 'task_status', '4', '进行中', 2);
INSERT INTO `t_status_flow` VALUES (9, 'task_status', '-1', '取消', 2);
INSERT INTO `t_status_flow` VALUES (10, 'task_status', '0', '未开始', 7);
INSERT INTO `t_status_flow` VALUES (11, 'task_status', '9', '已关闭', NULL);

SET FOREIGN_KEY_CHECKS = 1;
